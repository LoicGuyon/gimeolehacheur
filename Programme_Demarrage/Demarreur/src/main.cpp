#include <Arduino.h>
/* #define section 
  define is a useful C++ component that allows the programmer to give a name 
  to a constant value before the program is compiled. 
  Defined constants in arduino don’t take up any program memory space on the chip.
  The compiler will replace references to these constants with the defined value 
  at compile time.
  
  This can have some unwanted side effects though, if for example,
  a constant name that had been #defined is included in some other constant or variable name. In that case the text would be replaced by the #defined number (or text).

  In general, the const keyword is preferred for defining constants and should be used
  instead of #define. */

const int RELAIS = 10;   // tout ca est donc un peu casse gueule... 
const int C1 = 35 ;      // surtout quand tu définis des mots aussi courts que ca...
const int C2 = 37;
const int C3 = 38;
const int SD1 = 4;
const int SD2 = 21;
const int SD3 = 27;
const int IN1 = 25;
const int IN2 = 26;
const int IN3 = 23;
const int ANEMO = 22;

/* Définition des variables */

int dem = 0;          // condition de démarrage relative au vent 
int dem2 = 1;         //  condition de démarrage relative à la vitesse de rotation du rotor
int valeur_hall = 0;  // valeur de la position du rotor déduite des valeurs renvoyées par les capteurs à effet hall 

unsigned long T_min=1000;       // temps rebond rotor (µsecondes)
unsigned long TEchan=3000000;   // delai max en µs pour parcourir un secteur, si > arret theorique de la roue sur le secteur arrivée

float vitesse_vent = 0;   // vitesse de l'anémomètre
int Iane = 0;             // nb de top aimant
unsigned long Tane0 = 0;  // instant du premier top horloge anemo 
unsigned long Tane = 0;   // instant du dernier top horloge anemo

float vitesse_rot = 0;    // vitesse du rotor
int Irot=0;               // nb de top aimant
unsigned long Trot0 = 0;  // instant du premier top horloge aimant 
unsigned long Trot = 0;   // instant du dernier top horloge aimant 


void  wind() {
  if ((micros() - Tane)>= T_min) {  // delta T minimal ! 
    Tane = micros(); 
    Iane ++;
  }
}

void  rotatingspeed() {
  if ((micros() - Trot)>= T_min) {  // delta T minimal ! 
    Trot=micros(); 
    Irot ++;
  }
}  

void setup() {
  pinMode(C1,INPUT);    // COMMENETAIRE INDISPENSABLE!
  pinMode(C2,INPUT);
  pinMode(C3,INPUT);  
  pinMode(IN1,OUTPUT);
  pinMode(SD1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(SD2,OUTPUT);   //bisous fesses
  pinMode(IN3,OUTPUT);
  pinMode(SD3,OUTPUT);
  pinMode(RELAIS,OUTPUT);
  Serial.begin(115200);
  attachInterrupt( ANEMO, wind, RISING);
  attachInterrupt( C2, rotatingspeed, RISING);
}

void loop() {
  if (micros()-Tane0 > TEchan) {  // tempo en µsecondes pour tester si pas eu d'interruption = roue à l'arret sur entre deux aimants (theorie)
    if (Tane>Tane0)
      {vitesse_vent =  (0.08 * 6.28) / (( (float(Tane) - float(Tane0))/ (3600 * 1000 * float(Iane))));}
      else
      {vitesse_vent =  0;}
    Iane = 0;
    Tane0 = micros();
    Serial.print("vitesse vent = ");Serial.print(vitesse_vent,2);Serial.print(" [km/h]  - "); // tu vas afficher vitesse vent à chaque ligne du tableau, c'est bcp trop. pourquoi tu passes à la ligne ? 
    // delimiteur pour log tableur <-- ????
  }

  if (micros()-Trot0 > TEchan) { 
    if (Trot>Trot0)
      {vitesse_rot =  60/(16 * ((float(Trot) - float(Trot0))/ (1000000 * float(Irot))));}
      else
      {vitesse_rot =  0;}
    Serial.print("vitesse rotor = ");Serial.print(vitesse_rot);Serial.println(" [rpm]");
    Irot = 0;
    Trot0 = micros();
    vitesse_rot = 0;
  }
      
  if ((vitesse_rot < 50) && ( vitesse_vent > 5)){      // on envoie les consignes aux bobine pour accélérer l'éolienne. 
    digitalWrite(RELAIS, HIGH);
    int c1 = digitalRead (C1);          // vaut 1 quand Pole S devant B1 
    int c2 = digitalRead (C2);          // vaut 1 quand Pole S entre  B1 et B2
    int c3 = digitalRead (C3);          // vaut 1 quand Pole S davant B2
    valeur_hall =  c1+c2*2+c3*4;            //hall(c1,c2,c3);
      // valeur_hall = 1 <-> pos 1
      // valeur_hall = 2 <-> pos 3
      // valeur_hall = 3 <-> pos 2
      // valeur_hall = 4 <-> pos 5
      // valeur_hall = 5 <-> pos 6
      // valeur_hall = 6 <-> pos 4
      
       
    switch (valeur_hall)          // IL FAUT COMMENTER CETTE PARTIE
      {
      case 1 : // aimant entre 3 et 1
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,HIGH);
        break;
      
      case 3 : // aimant entre 
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,LOW);
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,HIGH);
        break;
      
      case 2 : 
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,LOW);
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,HIGH);
        break;    
      
      case 5 : 
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,HIGH);
        break;
      
      case 6 : 
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,LOW);
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,HIGH);
        break;
        
      case 4 : 
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,HIGH);
        break;
      //Tony petit zizi      
      }
  }
  if ((vitesse_rot > 50) || ( vitesse_vent < 5)) {
    digitalWrite(RELAIS,LOW);
  }
}
