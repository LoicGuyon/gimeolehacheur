/*
 * Ce programme permetra de freiner l'éolienne
 */
int relai_charge = 26;

int GPIO_mosfet_serie=23; // sortie PIN GPIO23 MOSFET ABAISSEUR
int alpha_abaisseur = 150;

int i;                  //variable commande moniteur série
byte incomingByte;      //variable commande moniteur série
int commande;           //variable lancement programme
 
void setup()
{
 Serial.begin(9600);
 pinMode(relai_charge,OUTPUT);
 digitalWrite(relai_charge,LOW); 
 pinMode(GPIO_mosfet_serie,OUTPUT);                //MOSFET est une sortie

 //initialisation des PWM
 ledcSetup(1,25000, 8);                            // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
 ledcAttachPin(GPIO_mosfet_serie, 1);             // affectation du pwm n°1 à la pin GPIO_mosfet_serie. Attention toutes les pin de l'ESP32 ne sont pas connectées aux CAN internes de l'ESP32. Si on veut utiliser le WIFI, un seul CAN peut fonctionner....

 //initialisation du rapport cyclique
  ledcWrite(1, alpha_abaisseur);                 //abaisseur (serie)

 //affichage dans le moniteur série des commandes à utiliser
  Serial.println("Pour démarrer le programme, appuyez sur la touche I");
  Serial.println("Pour arreter le programme, appuyez sur la touche O");
  Serial.println("");

}

void loop() 
{
  
    if (Serial.available() > 0) { //lecture dans le moniteur série 
                  // read the incoming byte: 
                  incomingByte = Serial.read();
  
                  // say what you got:
                  //Serial.print("I received: ");
                  //Serial.println(incomingByte, DEC);
                  if((incomingByte=='I')||(incomingByte=='i')) commande+=1; 
                  if((incomingByte=='O')||(incomingByte=='o')) commande-=1;        
  
      Serial.println("valeur de la commande : ");
      Serial.println(commande);
      Serial.println("");
    
    while (commande ==1)
    {
        Serial.println("Le freinage commence dans :");         
        Serial.println("3");
        delay(1000);
        Serial.println("2");
        delay(1000);
        Serial.println("1");
        delay(1000);
    
        digitalWrite(relai_charge,HIGH);
        alpha_abaisseur = 0;
         ledcWrite(1,alpha_abaisseur);
        Serial.println("Freinage ...");
    }  
    
     if (commande==0)
    {
        Serial.println("Le hachage commence dans :");         
        Serial.println("3");
        delay(1000);
        Serial.println("2");
        delay(1000);
        Serial.println("1");
        delay(1000);
    
        digitalWrite(relai_charge,LOW);
         alpha_abaisseur = 160;
         ledcWrite(1,alpha_abaisseur);
        Serial.println("HACHAGE ...");
      }
  }
}
