/* 
 * Cette partie du programme mesure la tension en enteée du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_tension_entree(float &tension_entree)            //le & veut dire que la variable est modifiable
{
 for (int i_Ve=0; i_Ve<N; i_Ve++)
  {
    sensorValue_Ve = analogRead(mVe);      //lecture Tension entrée     //mis en commentaire si l'on utilise la fonction random pour les tests
    tab_Ve[i_Ve]= sensorValue_Ve;
  }
 //On classe les valeurs du tableau dans l'ordre croissant
  for (int i_Ve=0; i_Ve<N; i_Ve++)
  {
    for (int j_Ve=i_Ve+1;j_Ve<N;j_Ve++)
    {
     if (tab_Ve[j_Ve]<tab_Ve[i_Ve])
     { 
      tmp_Ve=tab_Ve[i_Ve];
      tab_Ve[i_Ve]=tab_Ve[j_Ve];
      tab_Ve[j_Ve]=tmp_Ve;
     }
    }  
   }
  mediane_Ve = 0;
  for (int i_Ve=M;i_Ve<N-M; i_Ve++)
  {
    mediane_Ve = mediane_Ve + tab_Ve[i_Ve];
  }
  mediane_Ve = float(mediane_Ve)/(N-2*M);
  tension_entree = (float)((-0.00004*mediane_Ve*mediane_Ve)+(0.1344*mediane_Ve)+0.00000000000006);    //conversion Bit en V

}
