//sous-fonction permettant de faire varier le rapport cyclique alpha


 void variation_alpha(){

if((millis() - CompteurHorloge > PeriodeMAJalpha) && (Stop==0)){                     //temps à modifier en fonction du temps de stabilisation de l'éolienne. D'ailleurs c'est pas vraiment une tempo
  CompteurHorloge = millis();
 
    mesure_tension_entree(tension_entree);            
    mesure_courant_entree(courant_entree);
    mesure_courant_sortie(courant_sortie);
    mesure_tension_sortie(tension_sortie);

    i=i+1;

    Puissance_entree=tension_entree*courant_entree;
    Puissance_sortie=tension_sortie*courant_sortie;

      Alpha[i]=alpha;
      Uopt_entree[i]=tension_entree;
      Iopt_entree[i]=courant_entree;
      Popt_entree[i]=Puissance_entree;
      Uopt_sortie[i]=tension_sortie;
      Iopt_sortie[i]=courant_sortie;
      Popt_sortie[i]=Puissance_sortie;

      alpha=alpha+5;

      if(alpha>240){
        alpha=240;
        Stop=1;
        }

  }

        if(Stop==1 && Stop_final==0){

  for ( int i_i = 0; i_i <= (taille_tab-1); i_i++ )
  {
    if (Popt_entree[i_i] > d)
    {
      d = Popt_entree[i_i];
      id = i_i;
    }
  }

  Rendement=Popt_sortie[id]/Popt_entree[id];
  
  Serial.println("L'apprentissage est fini");
  Serial.print("Tension entrée ");
  Serial.print(Uopt_entree[id]);
  Serial.print("| Courant entrée ");
  Serial.print(Iopt_entree[id]);
  Serial.print("| Puissance entrée ");
  Serial.print(Popt_entree[id]);
  Serial.print("| Tension sortie ");
  Serial.print(Uopt_sortie[id]);
  Serial.print("| Courant sortie ");
  Serial.print(Iopt_sortie[id]);
  Serial.print("| Puissance sortie ");
  Serial.print(Popt_sortie[id]);
  Serial.print("| Rendement ");
  Serial.print(Rendement);
  Serial.print("| Alpha ");
  Serial.println(Alpha[id]);
  
  Stop_final=1;
  }
}
