/* 
 * Cette partie du programme mesure la tension en sortie du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau, on en retire les valeurs médiane et on en fait la moyenne
 */

void mesure_tension_sortie(float &tension_sortie)            //le & veut dire que la variable est modifiable
{
 for (int i_Vs=0; i_Vs<N; i_Vs++)
  {
    sensorValue_Vs = analogRead(mVs);      //lecture Tension entrée     //mis en commentaire si l'on utilise la fonction random pour les tests
    tab_Vs[i_Vs]= sensorValue_Vs;
  }
 //On classe les valeurs du tableau dans l'ordre croissant
  for (int i_Vs=0; i_Vs<N; i_Vs++)
  {
    for (int j_Vs=i_Vs+1;j_Vs<N;j_Vs++)
    {
     if (tab_Vs[j_Vs]<tab_Vs[i_Vs])
     { 
      tmp_Vs=tab_Vs[i_Vs];
      tab_Vs[i_Vs]=tab_Vs[j_Vs];
      tab_Vs[j_Vs]=tmp_Vs;
     }
    }  
   }
  mediane_Vs = 0;
  for (int i_Vs=M;i_Vs<N-M; i_Vs++)
  {
    mediane_Vs = mediane_Vs + tab_Vs[i_Vs];
  }
  mediane_Vs = float(mediane_Vs)/(N-2*M);
  tension_sortie = (float)((-0.00004*mediane_Vs*mediane_Vs)+(0.1344*mediane_Vs)+0.00000000000006);    //conversion Bit en V

}
