/*Code permettant de tester le rapport cyclique optimal pour une vitesse de vent fixe
On peut faire varier alpha de 0 à 255, 0 il ne hache pas, 255 les MOSFET reste fermés et laisse passer le courant
Il faut attendre que l'éolienne ait une vitesse de rotation stabilisé (que la tension soit stable) avant de récupérer les mesures*/

#include "variables_mesures.h"
#include "variables_hacheur.h"
#include "variables_apprentissage.h"


// définition des entrées
#define mIs 32
#define mVe 37
#define mIe 38
#define mVs 33

// définition des sorties
#define SD_barre 22                      // active ou desactive la commande des MOSFET (IR2104)
#define IN 23                           //commande des MOSFET (IR2104)
#define commande_Frein 26              //commande du relais Frein

void mesure_courant_entree(float &courant_entree); //appel les fonctions pour qu'on puisse les utiliser ensuite
void mesure_courant_sortie(float &courant_sortie);
void mesure_tension_entree(float &tension_entree);
void mesure_tension_sortie(float &tension_sortie);
void variation_alpha();
void moniteur();

void setup() {
  Serial.begin(9600);                               //vitesse d'affichage du moniteur série

  pinMode(mIs, INPUT);
  pinMode(mIe, INPUT);
  pinMode(mVe, INPUT);
  pinMode(mVs, INPUT);
  pinMode(SD_barre, OUTPUT);
  pinMode(IN, OUTPUT);                               
  pinMode(commande_Frein, OUTPUT);
  
  ledcSetup(1, 100000, 8);  // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
  ledcAttachPin(IN, 1);    //   affectation du pwm n°1 à la pin IN qui contrôle le hachage 

  digitalWrite(SD_barre, HIGH);
  ledcWrite(1, alpha);                                                        // alpha est appliqué au MOSFET
  
  delay(500);  
  Serial.println("Pour démarrer l'apprentissage :        appuyer sur G comme Go");
  Serial.println("Pour reinitialiser l'apprentissage :   appuyer sur R comme Reinitialiser");
}

void loop() { 
    
    moniteur(); 

    
  if (Start==1){
    ledcWrite(1, alpha);                                                        // alpha est appliqué au MOSFET
    variation_alpha();
  }

  if (ReInit==1) {    //on reinitialise le programme apprentissage
    ledcWrite(1, 0);
    alpha=80;
    id=-1;
    d=-1;
    Stop=0;
    Stop_final=0;
    i=-1;
  }
}
