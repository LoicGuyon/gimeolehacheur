//sous-programme permettant de dialoguer avec le moniteur serie et de nous renvoyer des informations

void moniteur() {

if (Serial.available() > 0) //lecture dans le moniteur série
   {                                                                
    incomingByte = Serial.read();                                  // par défaut, commande = 100
    if((incomingByte=='G')||(incomingByte=='g')) {commande=1;}    // on démarre le run
    if((incomingByte=='R')||(incomingByte=='r')) {commande=2;}   // on arrête le run 
    }

if (commande==1 && Start==0) {             // par défaut, start = 0
Serial.println("\n Chaud patate");       // le \n permet de revenir à la ligne dans le moniteur serie
ReInit=0;
Start=1;                                
commande=100;                           // pour éviter d'écrire ca en boucle !
}

if (commande==2 && Start==1) {
Serial.println("\n L'apprentissage est reinitialisé");
Start=0;
ReInit=1;
commande=100;
}

}
