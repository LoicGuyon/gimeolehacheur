/*
 * Cette partie du programme mesure le courant en entrée du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_courant_entree(float &courant_entree)
{
  //on lit 21 valeurs et on les places dans un tableau
  for (int i_Ie=0; i_Ie<N; i_Ie++)
  {
    sensorValue_Ie = analogRead(mIe);                      //lecture du courant d'entrée
    tab_Ie[i_Ie]= sensorValue_Ie;                               //on place la valeur du courant d'entrée dans le tableau
  }

  //On classe les valeurs du tableau dans l'ordre croissant   
  for (int i_Ie=0; i_Ie<N; i_Ie++)
  {
     for (int j_Ie=i_Ie+1;j_Ie<N;j_Ie++)
     {
        //on classe les valeurs par ordre croissant
        if (tab_Ie[j_Ie]<tab_Ie[i_Ie])
        { 
          tmp_Ie=tab_Ie[i_Ie];                                        //On lit la valeur de tab_Ie[i]
          tab_Ie[i_Ie]=tab_Ie[j_Ie];                                 //La valeur de tab_Ie[i] prend la valeur de tab_Ie{j]
          tab_Ie[j_Ie]=tmp_Ie;                                      //tab_Ie[j] prend la précédente valeur de tab_Ie[i]
        }
      }  
   }
  for (int i_Ie=M;i_Ie<N-M; i_Ie++)
  {
    mediane_Ie = mediane_Ie + tab_Ie[i_Ie];
  }
  mediane_Ie = float(mediane_Ie)/(N-2*M);
  courant_entree = (float)((-0.000006*mediane_Ie*mediane_Ie)+(0.0172*mediane_Ie)-7.892);      //Formule CAN, conversion Bit en A
}
