/*
 * cette partie du programme mesure le courant en sortie du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_courant_sortie(float &courant_sortie)
{
  //on classe 21 valeurs dans un tableau
  for (int i_Is=0; i_Is<N; i_Is++)
  {
    sensorValue_Is = analogRead(mIs); //lecture de la valeur du courant de sortie 
    tab_Is[i_Is]= sensorValue_Is;
  }

  
  for (int i_Is=0; i_Is<N; i_Is++)
  {
    for (int j_Is=i_Is+1;j_Is<N;j_Is++)
    {
      //on classe les valeurs par ordre croissant
      if (tab_Is[j_Is]<tab_Is[i_Is])
      { 
        tmp_Is=tab_Is[i_Is];                                      //On lit la valeur de tab_Ie[i]
        tab_Is[i_Is]=tab_Is[j_Is];                               //La valeur de tab_Ie[i] prend la valeur de tab_Ie{j]
        tab_Is[j_Is]=tmp_Is;                                    //tab_Ie[j] prend la précédente valeur de tab_Ie[i]
      }
    }  
  }
  
mediane_Is=tab_Is[10];                                         //la valeur médiane correspond à ma 10ème valeur

courant_entree = (float)((-0.000006*mediane_Ie*mediane_Ie)+(0.0172*mediane_Ie)-7.892);      //Formule CAN, conversion Bit en A
}
