#include "Arduino.h"              // utile pour ceux qui code via PlateformIo, plutôt que via Arduino IDE
#include "WiFi.h"                 // permet d'utiliser le service wifi de la carte
#include "ESPAsyncWebServer.h"    // permet de desynchroniser la gestion du serveur de la boucle loop: moins gourmand !
#include "SPIFFS.h"               // permet de stocker des fichiers sur l'esp.


//const char* ssid = "Bbox-D3B69147";       // entrer ici le SSID de la borne wifi 
const char* ssid = "Hugroïd";   
//const char* ssid = "CrowMhosom";   
unsigned long Compteur = 0;
//const char* password = "TicketToRide";     // et le mot de passe.
const char* password = "ettamere";     // et le mot de passe.
//const char* password = "azerty123.";     // et le mot de passe.

int Var_compteur = 0;                     // variable utilisée pour illustrer les interaction entre le code et la page HTML

// PARAMETRAGE LECTURE VITESSE
const int RELAIS = 10;   // tout ca est donc un peu casse gueule... 
const int C1 = 35 ;      // surtout quand tu définis des mots aussi courts que ca...
const int C2 = 37;
const int C3 = 38;
const int SD1 = 27; // 4;  <-- c'était faux ici ! 
const int SD2 = 4;  // 21; <-- c'était faux ici ! 
const int SD3 = 21; // 27; <-- c'était faux ici ! 
const int IN1 = 25;
const int IN2 = 26;
const int IN3 = 23;
const int ANEMO = 22;
// PARAMETRAGE MOTEUR
const byte sd_tab_1[] = {1,1,0,1,1,0,1,1,0,1,1,0};  // utilisé pour activer l'IR21 correspondant à la phase 1 
const byte sd_tab_2[] = {0,1,1,0,1,1,0,1,1,0,1,1};  // ... 2
const byte sd_tab_3[] = {1,0,1,1,0,1,1,0,1,1,0,1};  // ... 3
const byte in_tab_1[] = {1,1,2,0,0,2,1,1,2,0,0,2};  // utilié pour donner un sens positif ou nég à la phase correspondante 
const byte in_tab_2[] = {2,0,0,2,1,1,2,0,0,2,1,1}; //  ... 2 
const byte in_tab_3[] = {0,2,1,1,2,0,0,2,1,1,2,0}; //  ... 3

const bool SensDirect = true ; // true pour un sens de rotation direct, false pour l'indirect 
const byte Phase = 0;          // utilisé pour tester différentes phases à l'origine (tout les test possible entre 0 et 5).
const float SeuilVent = 10;    // vitesse limite en kmh pour démarrer le moteur 
const float SeuilRotation = 50;// vitesse limite en rpm pour stopper le moteur

/* Définition des variables */
bool DemarrageForce = 1;          // démarrage forcé du moteur (peut être commandé depuis la page internet
bool DemarrageStatut = 0;         // la machine est elle en train de démarrer
bool ConditionVent = 0;           // si la condition de vent pour le démarrage est remplie
bool ConditionRotation = 0;       // si la condition de rotation pour le démarrage est remplie
bool TestRampe = 0;               // si on veut faire la rampe: NE PAS UTILISE SANS DEBUGUER ARRET DEFINITIF
byte MoteurCompteur = 0;          // compteur du nb de fois que le moteur a été lancé.
unsigned long TempoMoteurCompteur = 0;  // tempo pour valider le fait que moteur est lancé.
unsigned long TimingMaxCompteur = 2000; // nb de secondes à partir des quelles on estime que le moteur est lancé
byte MoteurCompteurMAX = 1;       // nb de lancement max autorisé.
bool ArretDefinitifMoteur = 0;    // on ne relance plus le moteur quand cette variable vaut 1.


int valeur_hall = 0;  // valeur de la position du rotor déduite des valeurs renvoyées par les capteurs à effet hall 

unsigned long T_min = 1000;       // temps rebond rotor (µsecondes)
unsigned long TEchan = 3000000;   // delai max en µs pour parcourir un secteur, si > arret theorique de la roue sur le secteur arrivée

float vitesse_vent = 0;   // vitesse de l'anémomètre
int Iane = 0;             // nb de top aimant
unsigned long Tane0 = 0;  // instant du premier top horloge anemo 
unsigned long Tane = 0;   // instant du dernier top horloge anemo

float vitesse_rot = 0;    // vitesse du rotor
int Irot=0;               // nb de top aimant
unsigned long Trot0 = 0;  // instant du premier top horloge aimant 
unsigned long Trot = 0;   // instant du dernier top horloge aimant 

byte Pointeur = 0;             // utilisé pour désigner la position du pole S par rapport aux phases
unsigned long T_moteur = 100; // periode moteur
unsigned long T_moteur0 = 0;  // date dernier lancement

// DEFITION DES FONCTION INDEPENDANTES
void  wind() {
  if ((micros() - Tane)>= T_min) {  // delta T minimal ! 
    Tane = micros(); 
    Iane ++;
  }
}

void  rotatingspeed() {
  if ((micros() - Trot)>= T_min) {  // delta T minimal ! 
    Trot=micros(); 
    Irot ++;
  }
}  

AsyncWebServer server(80);                // Create AsyncWebServer object on port 80

// Fonction PROCESSOR <- permet d'intégrer les variables ARDUINO à la page HTML 

/* WIFI
String processor(const String& var){      // String&, c'est l'un des placeholder positionné dans le HMTL (entouré par des %)
//  Serial.println(var);                    // on écrit le nom de la variable dans le code HMTL
  if(var == "DemarrageStatut"){                     // et en fonction du nom de la variable, on agit !
    if (DemarrageStatut==1)
      {return "Moteur";}
      else {return "G&eacute;n&eacute;ratrice";}                       // on renvoit la valeur de la variable. cela va la remplacer dans le code HTML
  }
  if(var == "vitesse_vent"){
    return String(vitesse_vent);
  }
  if(var == "Var_compteur"){
    return String(Var_compteur);
  }
  if(var == "vitesse_rot"){
    return String(vitesse_rot);
  }
  if(var == "SensDirect"){
    if (SensDirect){return "positif";}
    else {return "négatif";}
  }
  if(var == "Phase"){
    return String(Phase);
  }
  if(var == "ConditionRotation"){
    return String(ConditionRotation);
  }
  if(var == "ConditionVent"){
    return String(ConditionVent);
  }
  return String();
}
*/ 

void setup(){
  // Serial port for debugging purposes
  //Serial.begin(9600);
  
  Serial.begin(115200);
  /* WIFI
  // pinMode(ledPin, OUTPUT);

  // Initialize SPIFFS
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(100);
    Serial.println("Connecting to WiFi..");
  }
  */
    // MESURES VITESSES
  pinMode(C1,INPUT);    // COMMENETAIRE INDISPENSABLE!
  pinMode(C2,INPUT);
  pinMode(C3,INPUT);  
  pinMode(IN1,OUTPUT);
  pinMode(SD1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(SD2,OUTPUT);   //bisous fesses
  pinMode(IN3,OUTPUT);
  pinMode(SD3,OUTPUT);
  pinMode(RELAIS,OUTPUT);
  attachInterrupt( ANEMO, wind, RISING);
  attachInterrupt( C2, rotatingspeed, RISING);
           
  /* WIFI
  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });

  server.on("/image", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/Fond1.jpg", "image/jpg");
  });

  // Route to set GPIO to HIGH
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    //digitalWrite(ledPin, HIGH);    
    DemarrageForce = 1 ;                    // dans cet exemple, on réinitialise le compteur quand STATE est ON. 
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to set GPIO to LOW
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    //digitalWrite(ledPin, LOW);    
    DemarrageForce = 0 ;                    // dans cet exemple, on réinitialise le compteur quand STATE est ON. 
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  // Start server
  server.begin();
  */
}
 
void loop(){
  if (micros()-Tane0 > TEchan) {  // tempo en µsecondes pour tester si pas eu d'interruption = roue à l'arret sur entre deux aimants (theorie)
    if (Tane>Tane0)
      {vitesse_vent =  (0.08 * 6.28) / (( (float(Tane) - float(Tane0))/ (3600 * 1000 * float(Iane))));}
      else
      {vitesse_vent =  0;}
    Iane = 0;
    Tane0 = micros();
    Serial.print("vitesse vent = ");Serial.print(vitesse_vent,2);Serial.print(" [km/h]  - "); // tu vas afficher vitesse vent à chaque ligne du tableau, c'est bcp trop. pourquoi tu passes à la ligne ? 
    // delimiteur pour log tableur <-- ????
  }

  if (micros()-Trot0 > TEchan) { 
    if (Trot>Trot0)
      {vitesse_rot =  60/(16 * ((float(Trot) - float(Trot0))/ (1000000 * float(Irot))));}
      else
      {vitesse_rot =  0;}
    Serial.print("vitesse rotor = ");Serial.print(vitesse_rot);Serial.println(" [rpm]");
    Irot = 0;
    Trot0 = micros();
  }
  if (!ArretDefinitifMoteur){
    if (TestRampe)
    {
      if (DemarrageForce)
      {
        digitalWrite(RELAIS,HIGH);
        if (millis()-T_moteur0>T_moteur)
        {
          Pointeur++;
          T_moteur0 = millis();
          Pointeur++;
          if (Pointeur==7){Pointeur=0;}
          digitalWrite(SD1,sd_tab_1[Pointeur]);
          if (sd_tab_1[Pointeur]){digitalWrite(IN1,in_tab_1[Pointeur + Phase]);}
          digitalWrite(SD2,sd_tab_2[Pointeur + Phase]);
          if (sd_tab_2[Pointeur]){digitalWrite(IN2,in_tab_2[Pointeur + Phase]);}
          digitalWrite(SD3,sd_tab_3[Pointeur + Phase]);
          if (sd_tab_3[Pointeur]){digitalWrite(IN3,in_tab_3[Pointeur + Phase]);}
          
          if (T_moteur>10){
            T_moteur=T_moteur-1;
            Serial.print("Pointeur = ");Serial.print(Pointeur);Serial.print("/6 - T_moteur = ");Serial.print(T_moteur);Serial.println("ms");
          }
          else {
            digitalWrite(RELAIS,LOW);
            DemarrageForce=0;
            Serial.println("DemarrageTerminé");
          }
        } 
      }  
    }
    else{
      if (vitesse_rot < SeuilRotation){ConditionRotation=1;}else{ConditionRotation=0;}
      if (vitesse_vent > SeuilVent){ConditionVent=1;}else{ConditionVent=0;}
      if (DemarrageForce || (ConditionRotation && ConditionVent)){      // on envoie les consignes aux bobine pour accélérer l'éolienne. 
        DemarrageStatut = 1;
        if (MoteurCompteur==0){
          TempoMoteurCompteur=millis();
          MoteurCompteur = 1;}
        else
          {if (millis()-TempoMoteurCompteur>TimingMaxCompteur){
            MoteurCompteur++; // on considère que le moteur a été vriament utilisé. 
            TempoMoteurCompteur=millis();
          }
        }
        digitalWrite(RELAIS, HIGH);
        int c1 = digitalRead (C1);          // vaut 1 quand Pole S devant B1 
        int c2 = digitalRead (C2);          // vaut 1 quand Pole S entre  B1 et B2
        int c3 = digitalRead (C3);          // vaut 1 quand Pole S davant B2
        valeur_hall =  c1+c2*2+c3*4;            //hall(c1,c2,c3);
        // Serial.print("c1 = ");Serial.print(c1);Serial.print("; c2 = ");Serial.print(c2);Serial.print("; c3 = ");Serial.print(c3);Serial.print("; Position = ");Serial.println(valeur_hall);
        switch (valeur_hall){
          case 1 : // aimant entre 3 et 1
            Pointeur = 0;break;
          case 3 :
            Pointeur = 1;break;
          case 2 :
            Pointeur = 2;break;
          case 6 : 
            Pointeur = 3;break;
          case 4 : 
            Pointeur = 4;break;
          case 5 : 
            Pointeur = 5;break;
        }
        if (SensDirect){
          digitalWrite(SD1,sd_tab_1[Pointeur + Phase]);
          if (sd_tab_1[Pointeur]){digitalWrite(IN1,in_tab_1[Pointeur + Phase]);}
          digitalWrite(SD2,sd_tab_2[Pointeur + Phase]);
          if (sd_tab_2[Pointeur]){digitalWrite(IN2,in_tab_2[Pointeur + Phase]);}
          digitalWrite(SD3,sd_tab_3[Pointeur + Phase]);
          if (sd_tab_3[Pointeur]){digitalWrite(IN3,in_tab_3[Pointeur + Phase]);}        
        }
        else {
          digitalWrite(SD1,sd_tab_1[Pointeur + Phase]);
          if (sd_tab_1[Pointeur]){digitalWrite(IN1,1 - in_tab_1[Pointeur + Phase]);}
          digitalWrite(SD2,sd_tab_2[Pointeur + Phase]);
          if (sd_tab_2[Pointeur]){digitalWrite(IN2,1 - in_tab_2[Pointeur + Phase]);}
          digitalWrite(SD3,sd_tab_3[Pointeur + Phase]);
          if (sd_tab_3[Pointeur]){digitalWrite(IN3,1 - in_tab_3[Pointeur + Phase]);}        
        }
      }     
      else {DemarrageStatut = 0;}
      if (!DemarrageForce && (!ConditionRotation || !ConditionVent)) {
        digitalWrite(RELAIS,LOW);
        if (MoteurCompteur>MoteurCompteurMAX){
          Serial.println("Phase Démarrage TERMINE !");
          ArretDefinitifMoteur=1;
        }
      }
    }
  }
  //Serial.print("Démarrage = ");Serial.println(DemarrageStatut);
}