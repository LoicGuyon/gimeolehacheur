// définition des variables
 
const int taille_tab = 25;      // longueur des vecteurs "tension" et "courant" issus de l'apprentissage.
                               //Tableaux issus de la courbe de puissance optimale 
float tab_Iopt[taille_tab] = {0.599, 0.864, 1.130, 1.395, 1.661, 1.926, 2.192, 2.457, 2.723, 2.988, 3.254, 3.519, 3.785, 4.050, 4.316, 4.581, 4.847, 5.112, 5.378, 5.4, 5.42, 5.44, 5.46, 5.48, 5.5};
float tab_Uopt[taille_tab] = {25, 27.5, 30, 32.5, 35, 37.5, 40, 42.5, 45, 47.5, 50, 52.5, 55, 57.5, 60, 62.5, 65, 67.5, 70, 72.5, 75, 77.5, 80, 82.5, 85};

int id = 0;                // initialisation de la variable qui stocke l'indice du point de la courbe le plus proche du point de fonctionnement de l'éolienne.
float d = -1;             // initialisation de la distance qui sépare le point de fonctionnement de la courbe.
int s = 0;               // signe +1 si le point est au dessus de la courbe. - sinon.
int Da;                 //Pas de variation d'alpha
float Dist;            //Distance entre le courant optimal et le courant d'entrée
 
float Idelta=0;      //égale à Dist    
float Udelta=0;     //égale à tension optimale moins tension entrée 
