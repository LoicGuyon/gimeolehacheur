/*
 * Cette partie du programme mesure le courant en sortie du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire les valeurs médiane et on en fait la moyenne
 */

void mesure_courant_sortie(float &courant_sortie)
{
  //on lit 21 valeurs et on les places dans un tableau
  for (int i_Is=0; i_Is<N; i_Is++)
  {
    sensorValue_Is = analogRead(mIs);                      //lecture du courant de sortie
    tab_Is[i_Is]= sensorValue_Is;                               //on place la valeur du courant de sortie dans le tableau
  }

  //On classe les valeurs du tableau dans l'ordre croissant   
  for (int i_Is=0; i_Is<N; i_Is++)
  {
     for (int j_Is=i_Is+1;j_Is<N;j_Is++)
     {
        //on classe les valeurs par ordre croissant
        if (tab_Is[j_Is]<tab_Is[i_Is])
        { 
          tmp_Is=tab_Is[i_Is];                                        //On lit la valeur de tab_Is[i]
          tab_Is[i_Is]=tab_Is[j_Is];                                 //La valeur de tab_Is[i] prend la valeur de tab_Is{j]
          tab_Is[j_Is]=tmp_Is;                                      //tab_Is[j] prend la précédente valeur de tab_Is[i]
        }
      }  
   }
  for (int i_Is=M;i_Is<N-M; i_Is++)
  {
    mediane_Is = mediane_Is + tab_Is[i_Is];
  }
  mediane_Is = float(mediane_Is)/(N-2*M);
  courant_sortie = (float)((-0.000006*mediane_Is*mediane_Is)+(0.0172*mediane_Is)-7.892);      //conversion Bit en A
}
