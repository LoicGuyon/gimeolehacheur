//sous-programme permettant de dialoguer avec le moniteur serie et de nous renvoyer des informations

void moniteur() {

if (Serial.available() > 0) //lecture dans le moniteur série
   {                                                                
    incomingByte = Serial.read();                                  // par défaut, commande = 100
    if((incomingByte=='G')||(incomingByte=='g')) {commande=1;}    // on démarre le run
    if((incomingByte=='S')||(incomingByte=='s')) {commande=2;}   // on arrête le run (et on freine)
    }

if (commande==1 && Start==0) {             // par défaut, start = 0
Serial.println("\n C'est parti !");       // le \n permet de revenir à la ligne dans le moniteur serie
Start=1;                                
commande=100;                           // pour éviter d'écrire ca en boucle !
}

if (commande==2 && Start==1) {
Serial.println("\n C'est fini les conneries");
Start=0;
commande=100;
}

}
