/*Code permettant de réguler (plutôt asservir) le rapport cyclique alpha
On peut faire varier alpha de 0 à 255, 0 il ne hache pas, 255 les MOSFET reste fermés et laisse passer le courant
*/

#include "variables_mesures.h"
#include "variables_hacheur.h"
#include "variables_apprentissage.h"


// définition des entrées
#define mIs 32
#define mVe 37
#define mIe 38
#define mVs 33

// définition des sorties
#define SD_barre 22                      // active ou desactive la commande des MOSFET (IR2104)
#define IN 23                           //commande des MOSFET (IR2104)
#define commande_Frein 26              //commande du relais Frein

void mesure_courant_entree(float &courant_entree); //appel les fonctions pour qu'on puisse les utiliser ensuite
void mesure_courant_sortie(float &courant_sortie);
void mesure_tension_entree(float &tension_entree);
void mesure_tension_sortie(float &tension_sortie);
void Distance(float* tab_Uopt, float* tab_Iopt, float tension_entree, float courant_entree, float &d, int &id, int &s, float &Dist,int &alpha);
void MinTable(float* Distances, int &id, float &d);
void variation_alpha(int &alpha);
void moniteur();

void setup() {
  Serial.begin(9600);                               //vitesse d'affichage du moniteur série

  pinMode(mIs, INPUT);
  pinMode(mIe, INPUT);
  pinMode(mVe, INPUT);
  pinMode(mVs, INPUT);
  pinMode(SD_barre, OUTPUT);
  pinMode(IN, OUTPUT);                               
  pinMode(commande_Frein, OUTPUT);
  
  ledcSetup(1, 100000, 8);  // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
  ledcAttachPin(IN, 1);    //   affectation du pwm n°1 à la pin IN qui contrôle le hachage 

  digitalWrite(SD_barre, HIGH);
  digitalWrite(commande_Frein, LOW);

  delay(500);  
  Serial.println("Pour démarrer la boucle de régulation : appuyer sur G comme Go");
  Serial.println("Pour stopper la régul :                 appuyer sur S comme Stop");
}

void loop() { 
    mesure_tension_entree(tension_entree);            //on fait tourner nos sous-fonction en boucle
    mesure_courant_entree(courant_entree);
    mesure_courant_sortie(courant_sortie);
    mesure_tension_sortie(tension_sortie);
    moniteur(); 

  if (tension_entree>25) {
     Start=1;                                                                            
    }
    
  if (Start==1 && Frein==0){
    Distance(tab_Uopt,tab_Iopt,tension_entree,courant_entree,d,id,s,Dist,alpha);           //On lance la boucle de régulation
    ledcWrite(1, alpha);                                                        // alpha est appliqué au MOSFET
  }
  
  if (Start==0 && Frein==0) {    //on désactive le hachage
    ledcWrite(1, 0);
    alpha=200;
  }

  /*if(sortie>30){              //si on débranche la charge, la tension en sortie n'est plus imposée, du coup on a une augmentation de la tension
  Frein=1;
    }
  }

  if (Frein==1){
    ledcWrite(1, 0);
    digitalWrite(commande_Frein, HIGH);
  }*/
  
}
