//sous-fonction permettant de faire varier le rapport cyclique alpha
//On fait varier alpha en fonction de la distance entre le courant optimal et le courant d'entrée, et en fonction de la valeur initiale d'alpha 

 void variation_alpha(int &alpha){

if(millis() - CompteurHorloge > PeriodeMAJalpha){                    
  CompteurHorloge = millis();
  
  if (alpha<100 && Dist>=1.5)                 
  {Da=2;}                                     
  if(alpha<150 && alpha>=100 && Dist>=1.5)
  {Da=5;}
  if(alpha>=150 && Dist>=1.5)
  {Da=10;}
  
  if (alpha<100 && Dist<1.5)
  {Da=1;}
  else if(alpha<150 && alpha>=100 && Dist<1.5)
  {Da=2;}
  if (alpha>=150 && Dist<1.5)
  {Da=5;}
  
  alpha=alpha-s*Da;
 
  if(alpha>220)                   //encadrement d'alpha
    {alpha=220;}
  if(alpha<130)
    {alpha=130;} 

  Serial.print("tension entrée ");                //Affichage des paramètres pris en compte dans la régulation
  Serial.print(tension_entree);
  Serial.print(" | courant entrée ");                                             
  Serial.print(courant_entree);
//  Serial.print(" | tension sortie ");
//  Serial.print(tension_sortie);
  Serial.print(" | Udelta ");
  Serial.print(Udelta);
  Serial.print(" | Idelta ");
  Serial.print(Idelta);
  Serial.print(" | s ");
  Serial.print(s);
  Serial.print(" | id ");
  Serial.print(id);
  Serial.print(" | Dist ");
  Serial.print(Dist);
  Serial.print(" | Uopt ");
  Serial.print(tab_Uopt[id]);
  Serial.print(" | Iopt ");
  Serial.print(tab_Iopt[id]);
  Serial.print(" | alpha ");
  Serial.println(alpha);
  
  }
}

void Distance(float* tab_Uopt, float* tab_Iopt, float tension_entree, float courant_entree, float &d, int &id, int &s, float &Dist,int &alpha){
  // (tab_Uopt, tab_Iopt) sont les vecteurs caractérisant respectivement l'abscisce et l'ordonnée de la courbe 
  // (tension_entree, courant_entree) sont les coordonnées du point dont on veut déterminer la position par rapport à la courbe
  // d      est la distance minimale entre le point tab_Uopt et la courbe tension_entree
  //Dist est la distance entre le point le courant optimal et le courant d'entrée
  // s      détermine si le point est au dessus (+1) ou en dessous (-1) de la courbe
    float Distances[taille_tab] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 
      for ( int i = 0; i <= (taille_tab-1); i++ )
        {
        Distances[i]=sqrt(pow(tab_Uopt[i]-tension_entree,2));             //Calcul des distances entre le point de fonctionnement de l'éolienne et chaques valeurs de la courbe optimale
        }
    MinTable(Distances, id, d); 
    Idelta = courant_entree-tab_Iopt[id];
    Udelta = tension_entree-tab_Uopt[id];
    Dist = sqrt(pow(tab_Iopt[id]-courant_entree,2));
      if (tab_Iopt[id]<courant_entree)                          //Si on est en dessous de la courbe optimale on augmente alpha, et inversement
        {s = +1 ;}
      else {s=-1;}
      variation_alpha(alpha);
}


void MinTable(float* Distances, int &id, float &d) {      //Fonction permettant de déterminer le point de la courbe optimale le plus proche de notre point de fonctionnement 
  d =-1;
  id=-1;
  for ( int i = 0; i <= (taille_tab-1); i++ )
  {
    if ( d < 0  || ( Distances[i] < d ))
    {
      d = Distances[i];
      id = i;
    }
  }
}
