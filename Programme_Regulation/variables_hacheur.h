//définition des variables utiles pour le hacheur

int alpha = 200;                //rapport cyclique MOSFET (il est compris entre 0 et 255 ce qui correspond à 0% et 100%)
int commande = 100;            //permet de gérer l'affichage de mesures et la variation de alfa (la valeur 100 correspond à la mise en repos de la variable)
byte incomingByte;            //variables pour lecture moniteur série 
int Start=0;                 //variable de démarrage 
int Frein=0;                //variable du frein

unsigned long CompteurHorloge = 0;        //Variable permettant la variation périodiquement
unsigned long PeriodeMAJalpha = 1000;     //Période de variation d'alpha
