/* .
 * dans ce fichier d'aide, on explique comment créer des tableau et on explique la fonction qui trouve la valeur max d'un tableau et renvoie son indice dans la table 
 */


float umin=25;      // premier valeur du tableau
float umax=200;     // dernière valeur du tableau (ou limite, si ca tombe pas exactement dessus)
float ustep=5.321;  // incrément du tableau 
int utaille=0;      // initialisation de la taille du tableau 

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);                        //vitesse d'affichage du moniteur série
}

void loop() {
  // put your main code here, to run repeatedly:
  utaille=(umax-umin)/ustep;  // taille des tableaux.
  float table_U[utaille]; // initialisation des tableaux
  float table_I[utaille];
  float table_P[utaille];
  
  for (int nI = 0; nI<utaille;nI++) // remplissage des tableaux.
    { 
      float UnI=umin+nI*ustep;
      table_U[nI]=UnI;
      float InI=UnI/2;
      table_I[nI]=InI;
      float PnI=InI*UnI;
      table_P[nI]=PnI;
 Serial.print("Indice: ");
  Serial.println(nI);
  Serial.print("Tension :  ");
  Serial.println(UnI);
  Serial.print("Intensité : ");
  Serial.println(InI);
      
    }
  Serial.print("U[0]: ");
  Serial.println(table_U[0]);
  Serial.print("U[1]: ");
  Serial.println(table_U[1]);
  Serial.print("I[1]: ");
  Serial.println(table_I[1]);
  Serial.print("P[1]: ");
  Serial.println(table_P[1]);

  float Xtest[4];
  Xtest[0]=3;
  Xtest[1]=30;
  Xtest[2]=2;
  Xtest[3]=200;
  int max_i = -1; // par défaut, l'indice du plus grand est -1 ! cette valeur sera mise à  jour par la fonction MaxTable
  float max_value = -1; // par défaut, la valeur max vaut -1 ! cette valeur sera mise à  jour par la fonction MaxTable
  MaxTable(Xtest, max_i, max_value);
  Serial.print("indice du plus grand: ");
  Serial.print(max_i);
  Serial.print(" - pour la valeur: ");
  Serial.println(max_value);


  float X[5]={0,1,2,3,4};
  float Y[5]={0,1,2,3,4};
  float x = 4;
  float y = 3.9;
  int id = 0;       // initialisation de la variable qui stocke l'indice du point de la courbe le plus proche du point de fonctionnement.
  float d = -1;     // initialisation de la distance qui sépare le point de fonctionnement de la courbe.
  int s = 0;        // signe +1 si le point est au dessus de la courbe. - sinon.
  Distance(X,Y,x,y,d,id,s);
  Serial.print(" la distance entre (x,y) et (X,Y): ");
  Serial.println(d);
  Serial.print(" le point de la courbe le plus proche est: ");
  Serial.println(id);
  Serial.print(" le signe est : ");
  Serial.println(s);
  delay(5000);
}

void Distance(float* X, float* Y, float x, float y, float &d, int &id, int &s){
  // (X, Y) sont les vecteurs caractérisant respectivement l'abscisce et l'ordonnée de la courbe 
  // (x, y) sont les coordonnées du point dont on veut déterminer la position par rapport à la courbe
  // d      est la distance minimale entre le point (x, y) et la courbe (X, Y)
  // s      détermine si le point est au dessus (+1) ou en dessous (-1) de la courbe
  float Distances[sizeof(X)] = {};
  for ( int i = 0; i <= sizeof(X); i++ )
    {
    Distances[i]=sqrt( (X[i]-x) * (X[i]-x) + (Y[i]-y) * (Y[i]-y) ) ;   
    // Serial.print(" la distance entre (x,y) et (X,Y) pour i = ");
    // Serial.print(i);Serial.print(" : ");
    // Serial.println(Distances[i]);
    }
   MinTable(Distances, id, d); 
   if (Y[id]<y) 
    {s = +1 ;}
    else {s=-1;}
}


void MinTable(float* X, int &min_i, float &min_v) {
  for ( int i = 0; i <= sizeof(X); i++ )
  {
    if ( min_v < 0  || ( X[i] < min_v ))
    {
      min_v = X[i];
      min_i = i;
    }
  }
}


void MaxTable(float* X, int &max_i, float &max_v) {
  for ( int i = 0; i <= sizeof(X); i++ )
  {
    if ( X[i] > max_v )
    {
      max_v = X[i];
      max_i = i;
    }
  }
}
