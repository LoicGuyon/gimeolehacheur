 
/*
 PROGRAMME TEST HACHEUR V2.0 27/02/19
 Ce programme sert à tester la carte HACHEUR.
Prérequis : brancher la tension d'alimentation continue +24V et brancher obligatoirement une charge en sortie.
La fréquence de hachage des 2 MOSFETs est contenu dans la variable frequence
Le rapport cyclique du mode abaisseur BUCK est contenu dans la variable alpha_abaisseur
Le rapport cyclique du mode elevateur BOOST est contenu dans la variable alpha_elevateur

Le fonctionnement est le suivant :

On suppose que la tension d'entrée du hacheur est de 20V, cela permettra de tester le mode abaisseur et ensuite le mode élévateur
 - au départ la tension de sortie est nulle car alpha_abaisseur=0 (alpha_elevateur=0 également)
 - ensuite alpha_abaisseur augmente de 0 à 255 (100%) donc la tension va croitre jusqu'à 20V.
 - ensuite alpha_abaisseur reste à 255 (MOSFET série conducteur) et alpha_elevateur augmente de 0 à valeur max correspondant à une tension de 40V (tension doublée)
 - la tension de 40V reste présente pendant 5 secondes ensuite la tension va decroitre jusqu'à 0V
 */
int frequence=10000; // definition de la frequence à 1000Hz par exemple

int alpha_elevateur=0; //rapport cyclique MOSFET SERIE (attention il est compris entre 0 et 255 ce qui correspond à 0% et 100%)
int alpha_abaisseur=0; //rapport cyclique MOSFET PARALELLE (attention il est compris entre 0 et 255 ce qui correspond à 0% et 100%)

int ancien_alpha_elevateur=0;
int ancien_alpha_abaisseur=0;

int GPIO_mosfet_serie=23; // sortie PIN GPIO23 MOSFET ABAISSEUR
int GPIO_mosfet_parallele=22; //sortie GPIO22 MOSFET ELEVATEUR

int relai_deconnexion_charge = 26;

int frein = 2;                      //variable commande moniteur série

  
void setup() {
 
  ledcSetup(1, frequence, 8); // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
  ledcAttachPin(GPIO_mosfet_serie, 1);  // affectation du pwm n°1 à la pin GPIO_mosfet_serie. Attention toutes les pin de l'ESP32 ne sont pas connectées aux CAN internes de l'ESP32. Si on veut utiliser le WIFI, un seul CAN peut fonctionner....
 
  ledcSetup(2, frequence, 8); // meme config pour le pwm n°2 ELEVATEUR.
  ledcAttachPin(GPIO_mosfet_parallele, 2);  // affectation du pwm n°2 à la pin GPIO_mosfet_parallele
  Serial.begin(9600);

  //initialisation des deux rapports cycliques
  ledcWrite(1, 0); //abaisseur (serie)
  ledcWrite(2, 0); // elevateur (parallele)

  pinMode(relai_deconnexion_charge,OUTPUT);         //relai deconnexion de la charge est une sortie
  digitalWrite(relai_deconnexion_charge,LOW);       //relai à l'état bas

}

void loop() {
  int i;
  byte incomingByte;

  if (Serial.available() > 0) { //lecture dans le moniteur série 
                // read the incoming byte: 
                incomingByte = Serial.read();

                // say what you got:
                //Serial.print("I received: ");
                //Serial.println(incomingByte, DEC);
                if((incomingByte=='p')&&(alpha_abaisseur==240)) alpha_elevateur+=10; 
                if((incomingByte=='m')&&(alpha_abaisseur==240)) alpha_elevateur-=10; 
                if((incomingByte=='a')&&(alpha_elevateur==0)) alpha_abaisseur+=10; 
                if((incomingByte=='q')&&(alpha_elevateur==0)) alpha_abaisseur-=10;   
                if((incomingByte=='F')||(incomingByte=='f')) 
                  {
                   alpha_abaisseur = 0;                 
                    frein=1; 
                  }      

                  if((incomingByte=='O')||(incomingByte=='o')) 
                  {
                                   
                    frein=3; 
                  }   
                }

  //limitation de alpha elevateur
  if(alpha_elevateur<=0) alpha_elevateur=0;
  if(alpha_elevateur>=130) alpha_elevateur=130; 
  
  // limitation de alpha abaisseur
  if(alpha_abaisseur<=0) alpha_abaisseur=0;
  if(alpha_abaisseur>=250) alpha_abaisseur=250;
  
  if((alpha_elevateur!=ancien_alpha_elevateur)||(alpha_abaisseur!=ancien_alpha_abaisseur)) {
          Serial.println("BOOST");  
          Serial.println(alpha_elevateur);
          Serial.println("BUCK");
          Serial.println(alpha_abaisseur);
          ancien_alpha_elevateur=alpha_elevateur;
          ancien_alpha_abaisseur=alpha_abaisseur;
          }

  ledcWrite(1, alpha_abaisseur); // met la valeur du rapport cyclique alpha sur le PWM ABAISSEUR
  ledcWrite(2, alpha_elevateur); // met la valeur du rapport cyclique alpha sur le PWM ELEVATEUR

   if(frein == 1)
   {
     
        Serial.println("Frein activé : ");
  
       digitalWrite(relai_deconnexion_charge,HIGH); //activation relai 
   }

   if(frein==3)
   {
    Serial.println("Frein désactivé : ");
  
       digitalWrite(relai_deconnexion_charge,LOW); //activation relai 
   }
}
