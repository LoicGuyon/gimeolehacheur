const int frqPin = 34; //Input Pin
const int oneSecond = 1000; //temps de mesure
int frqPinState = LOW;
int frq = 0;
int prevFrqPinState = LOW;
unsigned long timer = 0;
void setup()
{
 Serial.begin(9600);
 pinMode(frqPin, INPUT); 
}

void loop()
{
 frqPinState = digitalRead(frqPin); //lecture etat
 if (frqPinState == LOW) 
 {
   prevFrqPinState = LOW;
 }
 if (frqPinState == HIGH && prevFrqPinState == LOW) //compteur
 {
   prevFrqPinState = frqPinState;
   frq++;
 }
 if (millis() - timer > oneSecond) //Timer
 {
   timer = millis(); //Resets Time
   Serial.println(frq); //ecrire hz
   frq = 0; //resets hz
 }
}
 
