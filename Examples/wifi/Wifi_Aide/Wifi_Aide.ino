 // tout est expliqué ici : https://randomnerdtutorials.com/esp32-useful-wi-fi-functions-arduino/#3
// WiFi.mode(WIFI_STA)      station mode: the ESP32 connects to an access point 
// WiFi.mode(WIFI_AP)       access point mode: stations can connect to the ESP32
//                          plus de détail sur ce mode: https://randomnerdtutorials.com/esp32-access-point-ap-web-server/
// WiFi.mode(WIFI_STA_AP)   access point and a station connected to another access point

  
//  pour rajouter une image sur la page web: 
//  https://randomnerdtutorials.com/display-images-esp32-esp8266-web-server/ (ca prend pas mal de temps de se lancer la dedans !)
//    installer plateformIO IDE , indispensable pour développement un serveur html asynchrone sur espressif... https://platformio.org/install/ide?install=vscode
//    quelques conseil pour utiliser cette plateforme ici: https://docs.platformio.org/en/latest//integration/ide/vscode.html#quick-start
//    

#include <WiFi.h> // cette librairie est installée avec le module ESP32.

const char* ssid     = "ESP32-Les-Rois-Nes";
const char* password = "Thor";
WiFiServer server(80);  // c'est une histoire de porc
String header;          // Variable to store the HTTP request
String output26State = "off"; // Auxiliar variables to store the current output state
String output27State = "off";
const int output26 = 26;      // Auxiliar variables to store the current output state
const int output27 = 27;
int compteur = 0;
String z ="";

void setup() {
  Serial.begin(9600);
  // Initialize the output variables as outputs
  pinMode(output26, OUTPUT);
  pinMode(output27, OUTPUT);
  // Set outputs to LOW
  digitalWrite(output26, LOW);
  digitalWrite(output27, LOW);

  Serial.print("Setting AP (Access Point)…");   // Connect to Wi-Fi network with SSID and password
  WiFi.softAP(ssid, password);                  // Remove the password parameter, if you want the AP (Access Point) to be open
  
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
  server.begin();
}

void loop(){
  compteur = compteur +1;
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /26/on") >= 0) {
              Serial.println("GPIO 26 on");
              output26State = "on";
              digitalWrite(output26, HIGH);
            } else if (header.indexOf("GET /26/off") >= 0) {
              Serial.println("GPIO 26 off");
              output26State = "off";
              digitalWrite(output26, LOW);
            } else if (header.indexOf("GET /27/on") >= 0) {
              Serial.println("GPIO 27 on");
              output27State = "on";
              digitalWrite(output27, HIGH);
            } else if (header.indexOf("GET /27/off") >= 0) {
              Serial.println("GPIO 27 off");
              output27State = "off";
              digitalWrite(output27, LOW);
            }
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");
            
            // Web Page Heading
            char c[] = {compteur};
            String(z) = c;
            client.println("<body><h1>domaine des rois n&eacute;s, t=" + z + "</h1>");
            client.println("<body background=./Fond1.jpg>");
            
            // Display current state, and ON/OFF buttons for GPIO 26  
            client.println("<p>GPIO 26 - State " + output26State + "</p>");
            // If the output26State is off, it displays the ON button       
            if (output26State=="off") {
              client.println("<p><a href=\"/26/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/26/off\"><button class=\"button button2\">OFF</button></a></p>");
            } 
               
            // Display current state, and ON/OFF buttons for GPIO 27  
            client.println("<p>GPIO 27 - State " + output27State + "</p>");
            // If the output27State is off, it displays the ON button       
            if (output27State=="off") {
              client.println("<p><a href=\"/27/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/27/off\"><button class=\"button button2\">OFF</button></a></p>");
            }
            client.println("</body></html>");
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
