#include "Arduino.h"              // utile pour ceux qui code via PlateformIo, plutôt que via Arduino IDE
#include "WiFi.h"                 // permet d'utiliser le service wifi de la carte
#include "ESPAsyncWebServer.h"    // permet de desynchroniser la gestion du serveur de la boucle loop: moins gourmand !
#include "SPIFFS.h"               // permet de stocker des fichiers sur l'esp.

// Replace with your network credentials
//const char* ssid = "Bbox-D3B69147";       // entrer ici le SSID de la borne wifi 
const char* ssid = "Hugroïd";   
//const char* ssid = "CrowMhosom";   
unsigned long Compteur = 0;
//const char* password = "TicketToRide";     // et le mot de passe.
const char* password = "ettamere";     // et le mot de passe.
//const char* password = "azerty123.";     // et le mot de passe.

int Var_compteur = 0;                     // variable utilisée pour illustrer les interaction entre le code et la page HTML

// PARAMETRAGE LECTURE VITESSE
const int RELAIS = 10;   // tout ca est donc un peu casse gueule... 
const int C1 = 35 ;      // surtout quand tu définis des mots aussi courts que ca...
const int C2 = 37;
const int C3 = 38;
const int SD1 = 4;
const int SD2 = 21;
const int SD3 = 27;
const int IN1 = 25;
const int IN2 = 26;
const int IN3 = 23;
const int ANEMO = 22;

/* Définition des variables */
int valeur_hall = 0;  // valeur de la position du rotor déduite des valeurs renvoyées par les capteurs à effet hall 

unsigned long T_min=1000;       // temps rebond rotor (µsecondes)
unsigned long TEchan=3000000;   // delai max en µs pour parcourir un secteur, si > arret theorique de la roue sur le secteur arrivée

float vitesse_vent = 0;   // vitesse de l'anémomètre
int Iane = 0;             // nb de top aimant
unsigned long Tane0 = 0;  // instant du premier top horloge anemo 
unsigned long Tane = 0;   // instant du dernier top horloge anemo

float vitesse_rot = 0;    // vitesse du rotor
int Irot=0;               // nb de top aimant
unsigned long Trot0 = 0;  // instant du premier top horloge aimant 
unsigned long Trot = 0;   // instant du dernier top horloge aimant 

// DEFITION DES FONCTION INDEPENDANTES
void  wind() {
  if ((micros() - Tane)>= T_min) {  // delta T minimal ! 
    Tane = micros(); 
    Iane ++;
  }
}

void  rotatingspeed() {
  if ((micros() - Trot)>= T_min) {  // delta T minimal ! 
    Trot=micros(); 
    Irot ++;
  }
}  

AsyncWebServer server(80);                // Create AsyncWebServer object on port 80

// Fonction PROCESSOR <- permet d'intégrer les variables ARDUINO à la page HTML 
String processor(const String& var){      // String&, c'est l'un des placeholder positionné dans le HMTL (entouré par des %)
 // Serial.println(var);                    // on écrit le nom de la variable dans le code HMTL
  /*if(var == "STATE"){                     // et en fonction du nom de la variable, on agit !
    if(digitalRead(ledPin)){
      ledState = "ON";
      Var_compteur=0 ;                    // dans cet exemple, on réinitialise le compteur quand STATE est ON. 
    }
    else{
      ledState = "OFF";
    }
    Serial.print(ledState);             
    return ledState;                      // on renvoit la valeur de la variable. cela va la remplacer dans le code HTML
  }*/
  if(var == "vitesse_vent"){
    return String(vitesse_vent);
  }
  if(var == "vitesse_rot"){
    return String(vitesse_rot);
  }
  return String();
}


void setup(){
  // Serial port for debugging purposes
  //Serial.begin(9600);
  Serial.begin(115200);
  //pinMode(ledPin, OUTPUT);

  // Initialize SPIFFS
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");

  // MESURES VITESSES
  pinMode(C1,INPUT);    // COMMENETAIRE INDISPENSABLE!
  pinMode(C2,INPUT);
  pinMode(C3,INPUT);  
  pinMode(IN1,OUTPUT);
  pinMode(SD1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(SD2,OUTPUT);   //bisous fesses
  pinMode(IN3,OUTPUT);
  pinMode(SD3,OUTPUT);
  pinMode(RELAIS,OUTPUT);
  attachInterrupt( ANEMO, wind, RISING);
  attachInterrupt( C2, rotatingspeed, RISING);
  }

  // Print ESP32 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  // Route to load style.css file
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });

  // Route to set GPIO to HIGH
  /*server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, HIGH);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });*/
  
  // Route to set GPIO to LOW
  /*server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    digitalWrite(ledPin, LOW);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });*/

  // Start server
  server.begin();
}
 
void loop(){

    if (micros()-Tane0 > TEchan) {  // tempo en µsecondes pour tester si pas eu d'interruption = roue à l'arret sur entre deux aimants (theorie)
    if (Tane>Tane0)
      {vitesse_vent =  (0.08 * 6.28) / (( (float(Tane) - float(Tane0))/ (3600 * 1000 * float(Iane))));}
      else
      {vitesse_vent =  0;}
    Iane = 0;
    Tane0 = micros();
   // Serial.print("vitesse vent = ");Serial.print(vitesse_vent,2);Serial.print(" [km/h]  - "); // tu vas afficher vitesse vent à chaque ligne du tableau, c'est bcp trop. pourquoi tu passes à la ligne ? 
    // delimiteur pour log tableur <-- ????
  }

  if (micros()-Trot0 > TEchan) { 
    if (Trot>Trot0)
      {vitesse_rot =  60/(16 * ((float(Trot) - float(Trot0))/ (1000000 * float(Irot))));}
      else
      {vitesse_rot =  0;}
  //  Serial.print("vitesse rotor = ");Serial.print(vitesse_rot);Serial.println(" [rpm]");
    Irot = 0;
    Trot0 = micros();
  }
      
  if ((vitesse_rot < 50) && ( vitesse_vent >05)){      // on envoie les consignes aux bobine pour accélérer l'éolienne. 
    digitalWrite(RELAIS, HIGH);
    int c1 = digitalRead (C1);          // vaut 1 quand Pole S devant B1 
    int c2 = digitalRead (C2);          // vaut 1 quand Pole S entre  B1 et B2
    int c3 = digitalRead (C3);          // vaut 1 quand Pole S davant B2
    valeur_hall =  c1+c2*2+c3*4;            //hall(c1,c2,c3);
      // valeur_hall = 1 <-> pos 1
      // valeur_hall = 2 <-> pos 3
      // valeur_hall = 3 <-> pos 2
      // valeur_hall = 4 <-> pos 5
      // valeur_hall = 5 <-> pos 6
      // valeur_hall = 6 <-> pos 4
      
       
    switch (valeur_hall)          // IL FAUT COMMENTER CETTE PARTIE
      {
      case 1 : // aimant entre 3 et 1
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,HIGH);
        break;
      
      case 3 : // aimant entre 
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,LOW);
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,HIGH);
        break;
      
      case 2 : 
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,LOW);
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,HIGH);
        break;    
      
      case 5 : 
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,HIGH);
        break;
      
      case 6 : 
        digitalWrite(SD1,HIGH);
        digitalWrite(IN1,LOW);
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,HIGH);
        break;
        
      case 4 : 
        digitalWrite(SD3,HIGH);
        digitalWrite(IN3,LOW);
        digitalWrite(SD2,HIGH);
        digitalWrite(IN2,HIGH);
        break;
      //Tony petit zizi
      
      }
  }
  if ((dem == 0) || ( dem2 == 1)) {
    digitalWrite(RELAIS,LOW);
  }
}