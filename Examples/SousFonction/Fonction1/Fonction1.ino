void Fonction1(int &Var1, int Var2) { 
  // Fonction Bidon qui a deux vairable en entrée (note que les noms donnés à ces variables ne sont pas définis avant, mais uniquement ici...)
  // la premier est entrée en référence (avec un & devant) -> si elle est modifiée dans le coeur de cette fonction, ses modification seront conservées!
  // la deuxième est entrée sans référence -> les modification qui seront faites dans le coeur de cette fonction NE seront PAS conservées une fois sorti de la fonction!
  // ici, les modifs sont très simples:                  
  Var1++;
  Var2++;
}
