/*********
  tout ca vient de : 
  https://randomnerdtutorials.com/install-esp32-filesystem-uploader-arduino-ide/
    en résumé: 
    déposé dans le dossier "./data/" le contenu à téléverser
    cliquer sur "Outils > ESP32 Sketch Data Upload"
    notez que le téléversement efface tout ce qui était déposé avant dans l'espace de stockage.
    
  pour rajouter une image sur la page web: 
  https://randomnerdtutorials.com/display-images-esp32-esp8266-web-server/

    
  pour que les données entrées sur la page HTML renseignent des variables arduino
  https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/
 *********/

#include "SPIFFS.h"
 
void setup() {
  Serial.begin(9600);
  
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
    }
  
  File file = SPIFFS.open("/textExample.txt");
  if(!file){
    Serial.println("Failed to open file for reading");
    return;
  }
  
  Serial.println("File Content:");
  while(file.available()){
    Serial.write(file.read());
  }
  file.close();
}
 
void loop() {

}
