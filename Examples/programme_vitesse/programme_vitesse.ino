int Signal_GBF = 34; // Signal Analogique
/*const int nb_valeurs_Ir = 101; // nombre de valeurs dans le tableau
int tab_Ir[nb_valeurs_Ir];    // tableau regroupant les valeurs de courant
float tmp_Ir;*/
float periode;
unsigned long etat_haut;
unsigned long etat_bas;


void setup() {

// Configure le port série pour l'exemple
Serial.begin(9600);

// Met la broche de signal venant du GBF en entrée
pinMode(Signal_GBF, INPUT);
}

void loop() {




    // Mesure la durée de l'impulsion haute (timeout par défaut de 1s)
noInterrupts();
etat_haut = pulseIn(Signal_GBF, HIGH);
interrupts();

// Mesure la durée de l'impulsion basse (timeout par défaut de 1s)
noInterrupts();
etat_bas = pulseIn(Signal_GBF, LOW);
interrupts();

    // Calcul de la periode = etat haut + etat bas
 periode= (etat_bas + etat_haut);
   
  

// Calcul de la frequence = 1 / periode
float frequence = (1/ (periode*0.01));
if (etat_bas ==0 && etat_haut==0)
{
  frequence = 0;
}
int tr_minu = frequence*60;

for (int i = 0;i<100000;i++)
{
  Serial.println("Duree etat haut : ");
Serial.print(etat_haut);
Serial.println("");
Serial.println("Duree etat bas : ");
Serial.print(etat_bas);
Serial.println("");
Serial.println("Periode : ");
Serial.print(periode);
Serial.println("");
Serial.println("Frequence : ");
Serial.print(frequence);
Serial.println(" Hz");
Serial.print(tr_minu);
Serial.print(" tr/min");
Serial.println("");
Serial.println("");
}


for (int i = 0;i<30;i++)
{
  
}
//delay(1000);
}
