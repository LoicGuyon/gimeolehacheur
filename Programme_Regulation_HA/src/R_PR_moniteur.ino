//sous-programme permettant de dialoguer avec le moniteur serie et de nous renvoyer des informations

void moniteur() {

if (Serial.available() > 0) //lecture dans le moniteur série
   {                                                                
    incomingByte = Serial.read();                                 // par défaut, commande = 100
    if((incomingByte=='G')||(incomingByte=='g')) {commande=1;}    // on démarre le run
    if((incomingByte=='F')||(incomingByte=='f')) {commande=2;}    // on arrête le run (et on freine)
    }

if (commande==1 && Start==0) {                                    // par défaut, start = 0
//Serial.print("\n C'est parti !");       // le \n permet de revenir à la ligne dans le moniteur serie
Start=1;
Frein=0;                                // Par défaut, Frein = 0, sauf si on vient de freiner...
commande=100;                           // pour éviter d'écrire ca en boucle !
}

if (commande==2 && Start==1) {
//Serial.print("\n C'est fini les conneries");
//Serial.print("\n Le Frein est activé");
Frein=1;
Start=0;
commande=100;
}

}
