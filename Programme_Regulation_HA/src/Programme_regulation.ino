/*Code permettant de tester le rapport cyclique optimal pour une vitesse de vent fixe
On peut faire varier alpha de 0 à 255, 0 il ne hache pas, 255 les MOSFET reste fermés et laisse passer le courant
Il faut attendre que l'éolienne ait une vitesse de rotation stabilisé (que la tension soit stable) avant de récupérer les mesures*/

#include "Q_PR_variables_mesures.h"
#include "Q_PR_variables_hacheur.h"
#include "Q_PR_variables_apprentissage.h"


// définition des entrées
#define mIs 32
#define mVe 37
#define mIe 38
#define mVs 33

// définition des sorties
#define SD_barre 22                     // active ou desactive la commande des MOSFET (IR2104)
#define IN 23                           //commande des MOSFET (IR2104)
#define commande_Frein 26               //commande du relais Frein

void mesure_courant_entree(float &courant_entree); //appel les fonctions pour qu'on puisse les utiliser ensuite
void mesure_courant_sortie(float &courant_sortie);
void mesure_tension_entree(float &tension_entree);
void mesure_tension_sortie(float &tension_sortie);
void Distance(float* tab_Uopt, float* tab_Iopt, float tension_entree, float courant_entree, float &d, int &id, int &s);
void MinTable(float* Distance, int &id, float &d);
void variation_alpha();
void moniteur();


void setup() {
  Serial.begin(9600);                               //vitesse d'affichage du moniteur série
  
  pinMode(mIs, INPUT);
  pinMode(mIe, INPUT);
  pinMode(mVe, INPUT);
  pinMode(SD_barre, OUTPUT);
  pinMode(IN, OUTPUT);                               
  pinMode(commande_Frein, OUTPUT);
  
  ledcSetup(1, 100000, 8);  // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
  ledcAttachPin(IN, 1);   //   affectation du pwm n°1 à la pin IN qui contrôle le hachage 

  digitalWrite(SD_barre, HIGH);
  
  delay(500);  
  Serial.println("Pour démarrer la boucle de régulation : appuyer sur G comme Go");
  Serial.println("Pour activer le frein :                 appuyer sur F comme Frein");
}

void loop() { 
    mesure_tension_entree(tension_entree);            //on fait tourner nos sous-fonction en boucle
    mesure_courant_entree(courant_entree);
    mesure_courant_sortie(courant_sortie);
    mesure_tension_sortie(tension_sortie);
    moniteur(); 

  if (Start==1 && Frein==0) {
    digitalWrite(commande_Frein, LOW);                             
    Distance(tab_Uopt,tab_Iopt,tension_entree,courant_entree,d,id,s);       //On lance la boucle de régulation
    ledcWrite(1, alpha);                                                       // alpha est appliqué au MOSFET
    compteur_affichage=compteur_affichage+1;
    if(compteur_affichage>99){                                               //affiche les valeurs qu'à un certain intervalle de temps, évite que ça défile trop vite
      Serial.print("tension entrée ");
      Serial.print(tension_entree);
      Serial.print(" | courant entrée ");                                             
      Serial.print(courant_entree);
      Serial.print(" | courant sortie ");
      Serial.print(courant_sortie);
      Serial.print(" | tension sortie ");
      Serial.print(tension_sortie);
      Serial.print(" | alpha ");
      Serial.print(alpha);
      Serial.println();
      compteur_affichage=0;
    }
  }

  if ((Start==1 && Frein==1)||(Start==0 && Frein==1)) {    //on désactive le hachage et on active le frein
    ledcWrite(1, 0);
    digitalWrite(commande_Frein, HIGH);
  }
  
  if (courant_entree<0.2 && tension_sortie>28){       //si la charge est débranchée alors que l'éolienne tourne, le frein s'active (valeur à paufiner lors des tests)
    ledcWrite(1, 0);                                
    digitalWrite(commande_Frein, HIGH);
  }
}
