//sous-fonction permettant de faire varier le rapport cyclique alpha


void variation_alpha(){
  compteur_reg=compteur_reg+1;
  if(compteur_reg>199){                     //temps à modifier en fonction du temps de stabilisation de l'éolienne
    nouveau_Alpha=alpha+int(s*d*C);         // ATTENTION,  
    /* il faut sans doute normaliser le taux de variation.
      s*d*C/50 plutôt que S*d*C...

    "d" est la distance d'un point par rapport à une courbe 
    (principalement l'image du delta tension... si on est en Volt - Ampère) 
    255/alpha est le taux d'augmentation de la tension:
      si alpha = 1   ==> Ve = 255 * Vs
      si alpha = 127 ==> Ve = 2   * Vs
    L'incrément "da" ajouté à alpha doit donc dépendre de alpha. 
    il parait prudent de ne pas exceder une tension max d'~100V donc 
    limiter alpha à 60. "da" pourrait alors être d'autant plus grand 
    que alpha est proche de 255. Par exemple:

    if alpha<60
      {da = - 1*s;}
    else 
      {da = (50 - (255-alpha)*45/(255-80))*s;}
    
    pour proposer un pas "da" qui vaut 5 quand alpha = 80, et 50 quand alpha est proche de 255.
    quand d est faible (quel seuil?) on peut quoi qu'il en soit affiner et mettre da = 2. par exemple: 
     
    if (d<3)
      {nouveau_Alpha=alpha-s*2;} 
      else
      {nouveau_Alpha=alpha-s*(50 - (255-alpha)*45/(255-80));}
    
    */ 
    if(nouveau_Alpha>250)
      {nouveau_Alpha=250;}
    if(nouveau_Alpha<0)
      {nouveau_Alpha=0;}
    alpha=nouveau_Alpha; 
    compteur_reg=0;
  }
}

void Distance(float* tab_Uopt, float* tab_Iopt, float tension_entree, float courant_entree, float &d, int &id, int &s){
  // (tab_Uopt, tab_Iopt) sont les vecteurs caractérisant respectivement l'abscisce et l'ordonnée de la courbe 
  // (tension_entree, courant_entree) sont les coordonnées du point dont on veut déterminer la position par rapport à la courbe
  // d      est la distance minimale entre le point (tab_Uopt_m, tab_Iopt_m) et la courbe (tension_entree, courant_entree)
  // s      détermine si le point est au dessus (+1) ou en dessous (-1) de la courbe
  float Distances[sizeof(tab_Uopt)] = {};
  for ( int i = 0; i <= sizeof(tab_Uopt); i++ )
    {
    Distances[i]=sqrt( (tab_Uopt[i]-tension_entree) * (tab_Uopt[i]-tension_entree) + (tab_Iopt[i]-courant_entree) * (tab_Iopt[i]-courant_entree) ) ;   
    }
  MinTable(Distances, id, d); 
  if (tab_Iopt[id]<courant_entree) 
    {s = +1 ;}
  else 
    {s=-1;}
  variation_alpha();
}


void MinTable(float* Distance, int &id, float &d) {
  for ( int i = 0; i <= sizeof(Distance); i++ )
  {
    if ( d < 0  || ( Distance[i] < d ))
    {
      d = Distance[i];
      id = i;
    }
  }
}
