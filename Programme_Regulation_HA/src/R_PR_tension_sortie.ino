/*
 * Cette partie du programme mesure la tension en sortie du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_tension_sortie(float &tension_sortie)  
{
  //classe 11 valeurs dans un tableau 
  for (int i_Vs=0; i_Vs<11; i_Vs++)
  {
    sensorValue_Vs = analogRead(mVs);                 //lecture tension sortie
    tab_Vs[i_Vs]= sensorValue_Vs;                                   //on index la valeur de la tension de sortie dans le tableau
  }
 
  //On classe dans l'ordre croissant les valeurs du tableau
  for (int i_Vs=0; i_Vs<11; i_Vs++)
  {
   for (int j_Vs=i_Vs+1;j_Vs<11;j_Vs++)
   {
     if (tab_Vs[j_Vs]<tab_Vs[i_Vs])
     { 
      tmp_Vs=tab_Vs[i_Vs];
      tab_Vs[i_Vs]=tab_Vs[j_Vs];
      tab_Vs[j_Vs]=tmp_Vs;
     }
   }  
  }

  mediane_Vs=tab_Vs[5];
  tension_sortie = (float)(((mediane_Vs+173.59)/1280.9)*14.6); //utilisation de l'equation de la courbe caractéristique du CAN de l'ESP32 *Cst = coeff pont diviseur de tension + erreur resistance 

}
