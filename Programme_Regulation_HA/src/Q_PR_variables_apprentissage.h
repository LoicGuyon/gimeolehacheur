// définition des variables
 
const int taille_tab = 100;                   // longueur des vecteurs "tention" et "courant" issus de l'apprentissage.

float tab_Iopt[taille_tab] = {};    //reste à poursuivre les tests pour remplir ces deux tableaux
float tab_Uopt[taille_tab] = {};

int id = 0;       // initialisation de la variable qui stocke l'indice du point de la courbe le plus proche du point de fonctionnement de l'éolienne.
float d = -1;     // initialisation de la distance qui sépare le point de fonctionnement de la courbe.
int s = 0;        // signe +1 si le point est au dessus de la courbe. - sinon.
float C=1;      //constante à modifier en fonction de la stabilité de l'éolienne, plus C est grand et plus la variation de alpha sera grande pour un même écart à la courbe
int nouveau_Alpha=80;  //permet de modifier alpha

int compteur_reg=0;   
