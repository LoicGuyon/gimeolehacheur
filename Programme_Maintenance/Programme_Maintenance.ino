#include "variables_pont_diviseur_entree.h"
#include "variables_pont_diviseur_sortie.h"
#include "variables_mesure_courant_entree.h"
#include "variables_mesure_courant_sortie.h"

int frequence = 25000;                //fréquence de hachage 

int alpha_abaisseur=0;                //rapport cyclique MOSFET PARALELLE (attention il est compris entre 0 et 255 ce qui correspond à 0% et 100%)

int GPIO_mosfet_serie=23;             // sortie PIN GPIO23 MOSFET ABAISSEUR

int alpha_step = 0;                 //valeur du rapport cyclique imposé pour le hachage //MAX 251
int compteur =0;                      //temporisation

int relai_deconnexion_charge = 26;

float tension_entree_test = 19.5;     //à définir
float tension_sortie_test = 19.5;     //à définir
float courant_entree_test = 0.6;      //à définir
float courant_sortie_test = 0.6;      //à définir 

float tension_generateur  = 19.6;       //tension que founit le générateur triphasé 
float resistance = 15;                // valeur de la résistance de frein de l'éolienne en ohms 
float courant_charge = tension_generateur/resistance;  //courant théorique dans la charge 

byte  incomingByte;                  //variables pour commande moniteur série 
int commande = 2;                   //variable commande moniteur série
int frein = 2;                      //variable commande moniteur série


void setup() 
{
  Serial.begin(9600);
  pinMode(entree_Ve_hacheur, INPUT);                //mesure tension entrée est une entrée
  pinMode(sortie_Vs_hacheur, INPUT);                //mesure tension sortie est une sortie
  pinMode(entree_Ie, INPUT);                        //mesure courant entrée est une entrée 
  pinMode(sortie_Is, INPUT);                        //mesure courant sortie est une sortie
  pinMode(GPIO_mosfet_serie,OUTPUT);                //MOSFET est une sortie

  pinMode(relai_deconnexion_charge,OUTPUT);         //relai deconnexion de la charge est une sortie
  digitalWrite(relai_deconnexion_charge,LOW);       //relai à l'état bas
  
  //initialisation des PWM
  ledcSetup(1,frequence, 8);                            // definition du pwm n°1
  ledcAttachPin(GPIO_mosfet_serie, 1);             // affectation du pwm n°1 à la pin GPIO_mosfet_serie. 

  
  //initialisation des deux rapports cycliques
  ledcWrite(1, alpha_abaisseur);                 //abaisseur (serie)

   //affichage dans le moniteur série des commandes à utiliser
  Serial.println("Pour démarrer le programme, appuyez sur la touche I");
  Serial.println("Pour arreter le programme, appuyez sur la touche O");
  Serial.println("Pour commuter sur le frein, appuyez sur la touche F");
  Serial.println("");
  
}

void loop() 
{
  
  commande_moniteur();

   while (commande ==1 && frein ==0)
     {       
                
        //changement du rapport cyclique du hachage
         alpha_abaisseur = alpha_step;
         ledcWrite(1, alpha_abaisseur);                                     // met la valeur du rapport cyclique alpha sur le PWM ABAISSEUR


    
        for(int i; i<300;i++)
        {
          //tempo pour stabilisation des tensions et courant avant mesures
        }
        
          mesure_tension_entree(tension_entree);                               //lit la tension d'entrée du hacheur
          mesure_tension_sortie(tension_sortie);                               //lit la tension de sortie du hacheur
          mesure_courant_entree(courant_entree);                               //lit le courant d'entree du hacheur
          mesure_courant_sortie(courant_sortie);                               //lit le courant de sortie
          
          Serial.print("La tension d'entrée du hacheur est de : ");
          Serial.println(tension_entree);
          Serial.print("La tension de sortie du hacheur est de : ");
          Serial.println(tension_sortie);
          Serial.print("Le courant d'entrée du hacheur est de : ");
          Serial.println(courant_entree);
          Serial.print("Le courant de sortie du hacheur est de : ");
          Serial.println(courant_sortie);
    
    
          //Vérification bon fonctionnement pont diviseur de tension entrée hacheur 
          if (tension_entree>= tension_entree_test-1 && tension_entree<=tension_entree_test+1)
          {
            Serial.println("Mesure de la tension en entrée de l'abaisseur OK!");
          }
          else 
          {
            Serial.println("Mesure de la tension en entrée de l'abaisseur DEFAILLANT !");
          }
    
          if (tension_sortie>= tension_sortie_test-1 && tension_sortie<=tension_sortie_test+1)
          {
            Serial.println("Mesure de la tension en sortie de l'abaisseur OK!");
          }
          else
          {
            Serial.println("Mesure de la tension en sortie de l'abaisseur DEFAILLANT !");
          }
    
          if (courant_entree>= courant_entree_test - 0.1 && courant_entree<=courant_entree_test + 0.1)
          {
            Serial.println("Mesure de la courant en sortie de l'abaisseur OK!");
          }
          else 
          {
            Serial.println("Mesure de le courant en sortie de l'abaisseur DEFAILLANT !");
          }
    
    
           if (courant_sortie>= courant_sortie_test - 0.1 && courant_sortie<=courant_sortie_test - 0.1)
          {
            Serial.println("Mesure de la courant en sortie de l'abaisseur OK!");
          }
          else 
          {
            Serial.println("Mesure de le courant en sortie de l'abaisseur DEFAILLANT !");
          }
    
          Serial.println("");
         commande_moniteur(); //lecture commande moniteur série
         delay(1000);
     }
     
    /*
          if (tension_sortie == alpha_step*tension_entree)
          {
            Serial.print("Hachage fonction abaissage : operationnel");
          }
          else if (tension_sortie != alpha_step*tension_entree)
          {
            Serial.print("Hachage fonction abaissage : defectueux");
          }
   */

   while(frein == 1 && commande ==0)
   {
     
        Serial.println("Frein activé : ");
  
       digitalWrite(relai_deconnexion_charge,HIGH); //activation relai 
       
       for(int i; i<200; i++)
       {
        //tempo à laisser vide  
       }
    
       mesure_courant_entree(courant_entree);                               //lit le courant d'entree du hacheur
       mesure_courant_sortie(courant_sortie);                               //lit le courant de sortie
    
      Serial.print ("Le courant de la charge est de  : ");
      Serial.println (courant_charge);
      Serial.print ("Le courant en sortie du hacheur est de  : ");
      Serial.println(courant_sortie);
      Serial.println("");
         
       //vérification deconnexion de la charge 
       //on teste le courant entrée qui doit etre égale au courant charge et le courant de sortie doit être égale à 0A
      if (courant_entree>= courant_charge - 0.1 && courant_sortie<=courant_charge + 0.1 && courant_sortie ==0)
      {
        Serial.println("Deconnexion de la charge OK !");
      }
      else
     {
      Serial.println("Attention : problème frein éolienne");
     }
     Serial.println("");
    commande_moniteur();
    delay(1000);
   }

  while (frein == 0 && commande ==0)
  {
     
    Serial.println("Arret programme");// à laisser vide 
    delay(1000);
    commande_moniteur();
  }
 
        
 
}
