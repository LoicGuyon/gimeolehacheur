//Ce fichier contient les variables de la mesure de tension en sortie du hacheur

int sortie_Vs_hacheur = 33;    // On lit sur le pin 31
const int nb_valeurs_Vs = 11; // nombre de valeurs dans le tableau
int tab_Vs[nb_valeurs_Vs];    //tableau regroupant les valeurs de tension 
float tension_sortie;            //valeur tension sortie hacheur
float sensorValue_Vs;       // valeur du pont diviseur de la tension de sortie 
float mediane_Vs, tmp_Vs;   //variables tableaux pont diviseur de tension sortie hacheur 
//int j_Vs = 1;        // variables tableau pour classement ordre croissant
