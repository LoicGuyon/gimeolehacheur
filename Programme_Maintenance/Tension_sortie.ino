/*cette partie du programme mesure la tension en sortie du hacheur*/

void mesure_tension_sortie(float &tension_sortie)  
{
  //classe 11 valeurs dans un tableau 
  for (int i_Vs=0; i_Vs<11; i_Vs++)
  {
    sensorValue_Vs = analogRead(sortie_Vs_hacheur); //lecture tension sortie
    //sensorValue_Vs = random(3760);  
    tab_Vs[i_Vs]= sensorValue_Vs;
    //Serial.print(tab_Vs[i_Vs]);
    //Serial.print("\t");
  }
 
  
  for (int i_Vs=0; i_Vs<11; i_Vs++)
  {
   for (int j_Vs=i_Vs+1;j_Vs<11;j_Vs++)
   {
     if (tab_Vs[j_Vs]<tab_Vs[i_Vs])
     { 
      tmp_Vs=tab_Vs[i_Vs];
      tab_Vs[i_Vs]=tab_Vs[j_Vs];
      tab_Vs[j_Vs]=tmp_Vs;
     }
   }  
  }
  
  for (int i_Vs=0; i_Vs<11; i_Vs++)
  { 
   // Serial.print(tab_Vs[i_Vs]);//on affiche les valeurs du tableau
   // Serial.print("\t");
  }
// Serial.print("\n");
mediane_Vs=tab_Vs[5];
tension_sortie = (float)(((mediane_Vs+173.59)/1280.9)*14.6); //utilisation de l'equation de la courbe caractéristique du CAN de l'ESP32 *Cst = coeff pont diviseur de tension + erreur resistance 


//Serial.print ("La tension Vout est de : ");
//  Serial.println(tension_sortie)
//delay(1000);
}
