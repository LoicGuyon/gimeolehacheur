/* cette partie du programme mesure la tension en enteée du hacheur*/

void mesure_tension_entree(float &tension_entree)             //le & ne fonctionne pas, ça me marque une message d'erreur de déclaration dans le void loop
{
  
                                                           //classe 11 valeurs dans un tableau 
  for (int i_Ve=0; i_Ve<11; i_Ve++)
  {
    sensorValue_Ve = analogRead(entree_Ve_hacheur);      //lecture Tension entrée     //mis en commentaire si l'on utilise la fonction random pour les tests
    //sensorValue_Ve = random(3760);                         //on rentre une valeur aleatoire sur la valeur du pont diviseur de tension //utile quand on a pas le circuit sous la main pour les tests
    tab_Ve[i_Ve]= sensorValue_Ve;
   //Serial.print(tab_Ve[i_Ve]);                         //affiche les variables classées dans le tableau
   //Serial.print("\t");
  }
  //Serial.print("\n");
  for (int i_Ve=0; i_Ve<11; i_Ve++)
  {
    for (int j_Ve=i_Ve+1;j_Ve<11;j_Ve++)
    {
     if (tab_Ve[j_Ve]<tab_Ve[i_Ve])
     { 
      tmp_Ve=tab_Ve[i_Ve];
      tab_Ve[i_Ve]=tab_Ve[j_Ve];
      tab_Ve[j_Ve]=tmp_Ve;
     }
    }  
   }
  for (int i_Ve=0; i_Ve<11; i_Ve++)
  {
   // Serial.print(tab_Ve[i_Ve]);//on affiche les valeurs du tableau
   // Serial.print("\t");
  }
  
// Serial.print("\n");
mediane_Ve=tab_Ve[5];                                             //on récupère la valeur médiane
tension_entree = (float)(((mediane_Ve+173.59)/1280.9)*103.92); //utilisation de l'equation de la courbe caractéristique du CAN de l'ESP32 *Cst = coeff pont diviseur de tension + erreur resistance 

//Serial.print ("La tension Vin est de : ");    
//Serial.print(tension_entree);                                 //on lit la tension d'entree dans le moniteur série 
//Serial.print("\n");
//delay(1000);
  
}
