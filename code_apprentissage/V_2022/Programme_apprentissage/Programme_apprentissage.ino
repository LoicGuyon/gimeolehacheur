/*Code permettant de tester le rapport cyclique optimal pour une vitesse de vent fixe
On peut faire varier alpha de 0 à 255, 0 il ne hache pas, 255 les MOSFET reste fermés et laisse passer le courant
Il faut attendre que l'éolienne ait une vitesse de rotation stabilisé (que la tension soit stable) avant de récupérer les mesures*/

// définition des entrées
#define mIs 32
#define mVe 37
#define mIe 38
#define mVs 33

// définition des sorties
#define SD_barre 22                     // active ou desactive la commande des MOSFET (IR2104)
#define IN 23                           //commande des MOSFET (IR2104)
#define commande_Frein 26               //commande du relais Frein

#include "variables_mesures.h"
#include "variables_hacheur.h"

void mesure_courant_entree(float &courant_entree); //appel les fonctions pour qu'on puisse les utiliser ensuite
void mesure_courant_sortie(float &courant_sortie);
void mesure_tension_entree(float &tension_entree);
void mesure_tension_sortie(float &tension_sortie);
void variation_alpha();
void moniteur();


void setup() {
  Serial.begin(9600);                               //vitesse d'affichage du moniteur série

  pinMode(mIs, INPUT);
  pinMode(mIe, INPUT);
  pinMode(mVe, INPUT);
  pinMode(mVs, INPUT);
  pinMode(SD_barre, OUTPUT);
  pinMode(IN, OUTPUT);                               
  pinMode(commande_Frein, OUTPUT);
  digitalWrite(commande_Frein, LOW);
  
  ledcSetup(1, 50000, 8);  // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
  ledcAttachPin(IN, 1);   // affectation du pwm n°1 à la pin IN qui contrôle le hachage 

  delay(500);                                           //le delay permet d'afficher correctement le texte ci-dessous, sinon ça déconne       
  Serial.print("Pour démarrer le test appuyer sur Q");
  Serial.print("\nPour arrêter le test appuyer sur S");
  Serial.print("\nPour activer le frein appuyer sur W");
  Serial.print("\nPour désactiver le frein appuyer sur X");
  Serial.print("\nPour changer le pas appuyer sur O ou L");
  Serial.print("\nPour changer la valeur de alpha appuyer sur P ou M");
  Serial.print("\nPour afficher les valeurs des capteurs appuyer sur D");  
}

void loop() {
    mesure_tension_entree(tension_entree);            //on fait tourner nos sous-fonction en boucle 
    mesure_courant_entree(courant_entree);
    mesure_courant_sortie(courant_sortie);
    mesure_tension_sortie(tension_sortie);
    variation_alpha();
    moniteur(); 

  if (Start==1 && Frein==0) {                             //on peut faire varier la valeur de alpha
    digitalWrite(commande_Frein, LOW);
    digitalWrite(SD_barre, HIGH);                         //si SD_barre est HIGH les sorties de l'IR2104 sont activées et inversement (voir datasheet de l'IR2104)
    ledcWrite(1, alpha);  
  }

    if ((Start==1 && Frein==1)||(Start==0 && Frein==1))  {    //on désactive le hachage et on active le frein
    ledcWrite(1, 0);
    digitalWrite(SD_barre, LOW);
    digitalWrite(commande_Frein, HIGH);
  }

  if (Start==0 && Frein==0) {              //permet la remise à état initial
    digitalWrite(commande_Frein, LOW);
    digitalWrite(SD_barre, HIGH);
    ledcWrite(1, 80);                     //PWM 1 mis à environ 30% pour éviter que l'éolienne s'emballe
    alpha=80;
  }
}
