//sous-programme permettant de dialoguer avec le moniteur serie et de nous renvoyer des informations

void moniteur() {

if (Serial.available() > 0) //lecture dans le moniteur série
   {
    incomingByte = Serial.read();
    if((incomingByte=='D')||(incomingByte=='d')) {commande=0;}    //affiche les valeurs des capteurs et alpha 
    if((incomingByte=='O')||(incomingByte=='o')) {commande=1;}    //augmente la valeur du pas
    if((incomingByte=='L')||(incomingByte=='l')) {commande=2;}    //diminue la valeur du pas
    if((incomingByte=='P')||(incomingByte=='p')) {commande=3;}    //augmente alpha
    if((incomingByte=='M')||(incomingByte=='m')) {commande=4;}    //diminue alpha
    if((incomingByte=='Q')||(incomingByte=='q')) {commande=5;}    //démarre le programme de test
    if((incomingByte=='S')||(incomingByte=='s')) {commande=6;}    //arrête le programe de test
    if((incomingByte=='W')||(incomingByte=='w')) {commande=7;}    //active le Frein
    if((incomingByte=='X')||(incomingByte=='x')) {commande=8;}    //désactive le Frein
    }

if (commande==5 && Start==0) {
Serial.print("\nLe test de hachage commence dans 3");       //le \n permet de revenir à la ligne dans le moniteur serie
delay(1000);
Serial.print("\n                                 2");
delay(1000);
Serial.print("\n                                 1");
delay(1000);
Start=1;
commande=100;
}

if (commande==6 && Start==1) {
Serial.print("\nLe test de hachage se termine dans 3");
delay(1000);
Serial.print("\n                                   2");
delay(1000);
Serial.print("\n                                   1");
delay(1000);
Start=0;
commande=100;
}

if (commande==7 && Frein==0) {
Serial.print("\nLe Frein est activé");
Frein=1;
commande=100;
}

if (commande==8 && Frein==1) {
Serial.print("\nLe Frein est désactivé");
Frein=0;
commande=100;
}

if (commande==0)
{
 //On affiche les valeurs des capteurs et le rapport cyclique alpha dans le moniteur
mesure_tension_entree(tension_entree); 
Serial.print("\nLa tension en entrée est de ");
Serial.print(tension_entree);
Serial.println(" V");
mesure_courant_entree(courant_entree);
Serial.print("Le courant en entrée est de ");
Serial.print(courant_entree);
Serial.println(" A");
mesure_tension_sortie(tension_sortie);
Serial.print("La tension en sortie est de ");
Serial.print(tension_sortie);
Serial.println(" V");
mesure_courant_sortie(courant_sortie);
Serial.print("Le courant en sortie est de ");
Serial.print(courant_sortie);
Serial.println(" A");
variation_alpha();
Serial.print("Le rapport cyclique alpha est à ");
Serial.println(alpha);
commande=100; 
}

if (ancien_pas!=pas) {      //affiche le pas que si on change sa valeur (évite d'afficher une longue liste)
Serial.print("\npas = ");
Serial.println(pas);
ancien_pas=pas;
}

if (ancien_alpha!=alpha) {      //affiche le rapport cyclique alpha que si on change sa valeur 
Serial.print("\nalpha = ");
Serial.println(alpha);
ancien_alpha=alpha;
}
}
