// définition des variables

 
          //mesure courant Is

//Variables entrée
float sensorValue_Is;                 // variable qui stocke le courant de sortie

// Tableau permettant de coder un filtre médian
const int N = 101; // taille tableau
const int M = 20; // nombre de point traité
const int nb_valeurs_Is = 101;      // nombre de valeurs dans le tableau
int tab_Is[nb_valeurs_Is];        // tableau de valeurs
float mediane_Is, tmp_Is;        // valeur numé médiane du tableau, variable locale de la méthode (on s'en fout)

//Variable sortie 
float courant_sortie;         // valeur analogique médiane 

         //mesure courant Ie
float sensorValue_Ie;
const int nb_valeur_Ie = 101;
int tab_Ie[nb_valeur_Ie];
float mediane_Ie, tmp_Ie;
float courant_entree;
 
          //mesure tension Ve
float sensorValue_Ve;        
const int nb_valeurs_Ve = 101;
int tab_Ve[nb_valeurs_Ve];   
float mediane_Ve, tmp_Ve;   
float tension_entree;       

          //mesure tension Vs
float sensorValue_Vs;        
const int nb_valeurs_Vs = 101;
int tab_Vs[nb_valeurs_Vs];   
float mediane_Vs, tmp_Vs;   
float tension_sortie;   
