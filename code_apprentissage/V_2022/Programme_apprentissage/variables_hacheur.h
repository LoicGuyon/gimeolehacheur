//définition des variables utiles pour le hacheur

int alpha = 80;                 //rapport cyclique MOSFET (il est compris entre 0 et 255 ce qui correspond à 0% et 100%)
int commande = 100;            //permet de gérer l'affichage de mesures et la variation de alfa (la valeur 100 correspond à la mise en repos de la variable)
byte incomingByte;            //variables pour lecture moniteur série 
int selec_pas =1;            //variable pour sélectionner la valeur du pas (1, 5 ou 10)
int pas=5;                  //pas pour faire varier alfa
int ancien_alpha=80;       //permet d'afficher la valeur de alpha que quand on fait varier celle-ci
int ancien_pas=1;         //permet d'afficher la valeur du pas que quand on modifie celle-ci
int Start=0;             //variable de démarrage 
int Frein=0;            //variable du frein
