/* Programme principal permettant de déterminer la courbe de puissance optimale d'une éolienne pour une vitesse de vent stabilisée. 
 *Ce programme à pour but de déterminer la meilleure puissance que l'éolienne peut fournir à une vitesse de vent donné
 *Les valeurs devront être relevées puis entrées sur un fichier excel indépendant
 */


//déclaration des variables
#include "variables_pont_diviseur_entree.h"         //on inclut la bibliothèque contenant les variables de mesure de la tension en entree du hacheur
#include "variables_pont_diviseur_sortie.h"         //on inclut la bibliothèque contenant les variables de mesure de la tension en sortie du hacheur
#include "variables_mesure_courant_entree.h"        //on inclut la bibliothèque contenant les variables de mesure du courant d'entree
#include "variables_mesure_courant_sortie.h"        //on inclut la bibliothèque contenant les variables de mesure du courant de sortie
#include "variables_fonction_stabilite.h"           //on inclut la bibliothèque contenant les variables utilisées dans la fonction stabilité
#include "variables_fonction_puissance_optimale.h" //on inclut la bibliothèque contenant les variables utilisées dans la fonction qui determinera la puissance optimale
#include "variables_demarrage.h"                   //on inclut la bibliothèque contenant les variables utilisées dans la fonction du demarreur

float tension_opt;
float intensite_opt;
float puissance_opt;
int relai_deconnexion_charge = 26;
int commande = 6;                                 //variable commande moniteur série

// définition des prototypes de fonction
void commande_moniteur();
void puissance_optimale(float &UnI, float &InI, float &PnI, int &tps_stabilisation);
void mesure_tension_entree(float &tension_entree);
void mesure_courant_entree(float &courant_Iin);
void mesure_tension_sortie(float &tension_sortie);
void mesure_courant_sortie(float &courant_Iout);
void MaxTable(float* X, int &max_i, float &max_v);
void stabilite(float Uconsigne, float &courant_Iin, int &temps_total);

void setup() {
  //Vitesse affichage moniteur serie
 Serial.begin(9600);                               //vitesse d'affichage du moniteur série

 //Déclaration des capteurs
 pinMode(entree_Ve_hacheur, INPUT);                //mesure tension entrée est une entrée
 pinMode(sortie_Vs_hacheur, INPUT);                //mesure tension sortie est une sortie
 pinMode(entree_Ie, INPUT);                        //mesure courant entrée est une entrée 
 pinMode(sortie_Is, INPUT);                        //mesure courant sortie est une sortie


 // Variables Frein
 pinMode(relai_deconnexion_charge,OUTPUT);         //relai deconnexion de la charge est une sortie
 digitalWrite(relai_deconnexion_charge,LOW);       //relai à l'état bas
digitalWrite(22, HIGH);

 //Déclaration des MOSFETs
 pinMode(GPIO_mosfet_serie,OUTPUT);                //MOSFET est une sortie
 pinMode(GPIO_mosfet_parallele, OUTPUT);          //MOSFET est une sortie
 
 //initialisation des PWM
 ledcSetup(1,frequence, 8);                       // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
 ledcAttachPin(GPIO_mosfet_serie, 1);             // affectation du pwm n°1 à la pin GPIO_mosfet_serie. Attention toutes les pin de l'ESP32 ne sont pas connectées aux CAN internes de l'ESP32. Si on veut utiliser le WIFI, un seul CAN peut fonctionner....

 //initialisation des deux rapports cycliques
  ledcWrite(1, alpha_abaisseur);                 //abaisseur (serie)

  
  //affichage dans le moniteur série des commandes à utiliser
  Serial.println("Pour démarrer le programme, appuyez sur la touche I");
  Serial.println("Pour arreter le programme, appuyez sur la touche O");
  Serial.println("Pour freiner l'éolienne, appuyez sur la touche F");
  Serial.println("Pour laisser tourner l'éolienne libre, appuyez sur la touche L");
  Serial.println("Pour lancer le demarreur de l'éolienne, appuyer sur la touche D");
  Serial.println("");

  }

  
void loop() 
{
   commande_moniteur(); //lecture dans le moniteur série
   /*Arret du programme*/
   if (commande ==0)
    {    
      ledcWrite(1,50);      
      commande_moniteur(); //lecture dans le moniteur série
      Serial.println ("Arret du programme");      
    }
   
   /*Demarrage programme meilleur puissance*/
   while (commande==1)
   {  
    Serial.println("Le programme commence dans :");         
    Serial.println("3");
    delay(1000);
    Serial.println("2");
    delay(1000);
    Serial.println("1");
    delay(1000);
  
    puissance_optimale(tension_opt, intensite_opt, puissance_opt, tps_stabilisation);

    //Acquisition des données des capteurs
    mesure_tension_entree(tension_entree);                               //lit la tension d'entrée du hacheur
    mesure_tension_sortie(tension_sortie);                               //lit la tension de sortie du hacheur
    mesure_courant_entree(courant_entree);                                  //lit le courant d'entree du hacheur
    mesure_courant_sortie(courant_sortie);                               //lit le courant de sortie

    //Affichage de l'acquisition des capteurs dans le moniteur série
    Serial.print("tension en entrée : "); Serial.println(tension_entree);
    Serial.print("tension en sortie : "); Serial.println(tension_sortie);
    Serial.print("courant en entree : "); Serial.println(courant_entree);
    Serial.print("courant en sortie : "); Serial.println(courant_sortie);
    Serial.print("tension optimale: ");   Serial.println(tension_opt);
    Serial.print("Intensité optimale : ");Serial.println(intensite_opt);
    Serial.print("Puissance optimale : ");Serial.println(puissance_opt);
    Serial.print("Temps de stabilisation");Serial.println(tps_stabilisation);
    Serial.println("");   
    Serial.println("");
    Serial.println("fin programme");
    Serial.println("");
    Serial.println("");

   
    commande_moniteur();                                        //lecture dans le moniteur série
    ledcWrite(1,80);                                            //On détermine un certain rapport cyclique pour garder en sécurité l'éolienne
    commande = 4;                                               //on lance la commande fin du programme du hachage
   } 
   
    /*Frein*/
   if(commande ==2)
   {
    digitalWrite(relai_deconnexion_charge,HIGH);                //activation relai 
    Serial.println("Frein Activé");
    delay(500);
    commande_moniteur();                                      //lecture dans le moniteur série
   }

   /*Desactivation du hachage*/
   if(commande ==3)
   {
    digitalWrite(relai_deconnexion_charge,LOW);
    Serial.println("Desactivation du hachage");
    ledcWrite(1,0);   
    delay(500);
    commande_moniteur();//alpha = 0 pour abaisseur
   }
   
   /*Fin du programme hachage*/
   if (commande ==4)
    {
       ledcWrite(1,50);
      commande_moniteur(); //lecture dans le moniteur série 
    }

       /*Fin du programme hachage*/
   if (commande ==5)
    {
      digitalWrite(moteur,HIGH);
      delay(1000);
      digitalWrite(electro_aimant,HIGH);
      commande_moniteur(); //lecture dans le moniteur série 
      
      /* Demarreur de l'éolienne*/
    if (commande != 5)
      {
        digitalWrite(electro_aimant,LOW);
        delay(1000);
        digitalWrite(moteur,LOW);
      }
    }
}
