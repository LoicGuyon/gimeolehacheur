/*
 * Ce fichier annonce les variables qui vont �tre utilis�es pour la lecture de la tension d'entr�e du hacheur'
 */

//Variables entrée
int entree_Ve_hacheur = 37;    //  On lit sur le pin 37
float sensorValue_Ve;        // valeur du pont diviseur de tension 


//Variables classement tableau
const int nb_valeurs_Ve = 11; // nombre de valeurs dans le tableau 
int tab_Ve[nb_valeurs_Ve];    //tableau regroupant les valeurs de tensions d'entr�e
float mediane_Ve, tmp_Ve;    //variables tableau 


//Variables sortie
float tension_entree;             //Valeur tension entr�e hacheur
