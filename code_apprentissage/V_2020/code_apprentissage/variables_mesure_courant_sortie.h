/*
 * Ce fichier contient les variables permettant la lecture du courant de sortie du hacheur
 */

//Variables entree
int sortie_Is = 32;					// On lit sur le pin 30
float sensorValue_Is;        // variable to store the value coming from the sensor


//Classement tableau
const int nb_valeurs_Is = 21;		// nombre de valeurs dans le tableau
int tab_Is[nb_valeurs_Is];			// tableau � de valeurs
float mediane_Is, tmp_Is;			//variable tableau capteur courant sortie


//Variables sortie
float courant_sortie;              //valeur sortie d�cimale
