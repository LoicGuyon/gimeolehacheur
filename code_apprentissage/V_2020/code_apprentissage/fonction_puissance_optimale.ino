/*
 * cette partie du programme donner la meilleure puissance (ainsi que sa tension et son courant)
 */

/*
 * On part d'une tension haute que l'on abaissera
 */
void puissance_optimale(float &UnI, float &InI, float &PnI, int &tps_stabilisation)
{
  UnI = 0;
  utaille=(umax-umin)/upas;                      // taille des tableaux.
  
  float table_U[utaille];                       // initialisation des tableaux
  float table_I[utaille];
  float table_P[utaille];
  for (int nI = 0; nI<utaille;nI++)            // remplissage des tableaux.
    { 
    
      alpha_abaisseur = 0;

      commande_moniteur(); //lecture dans le moniteur série
      
      UnI=umax-nI*upas;                        
      table_U[nI]=UnI;                                   //On classe la tension consigne dans une tableau  
      
      InI=0;                                           //on initialise le courant d'entrée à zéro avant la mesure
      tps_stabilisation = 0;                          //on initialise le temps de stabilisation à 0
      Serial.print("tension en entrée stabilite : ");
      Serial.println(UnI);
      
      stabilite(UnI,InI, tps_stabilisation);         // appelle la fonction stabilité (avec tout les paramètres qui vont bien),UnI correspond au Uconigne dans la fonction stabilité
          
      Serial.print("courant en sortie stabilite : ");
      Serial.println(InI);                             // la suite du code de puissance_optimale n'est pas exécutée tant que stabilité n'a pas terminé son travail !! (attention aux boucles infinies)
                          
      table_I[nI]=InI;                                 //récupère l'intensité en fonction de la tension de la boucle stabilite 
      
      PnI=InI*UnI;                                    //on determine la puissance en faisant P=U*I
      table_P[nI]=PnI;                               //on classe la valeur de la puissance dans le tableau
      
      Serial.print("Tension UnI");
      Serial.println(UnI);
      Serial.print("Intensité InI");
      Serial.println(InI);
      Serial.print("Puissance PnI");
      Serial.println(PnI);  
      Serial.print("Temps de stabilisation");
      Serial.println(tps_stabilisation); 
      Serial.println(""); 
    }


  
  //renvoie la meilleure puissance
  MaxTable(table_P, max_i, max_value);                                  //appel de la fonction pour déterminer la puissance optimale 

  UnI = table_U[max_i];
  InI = table_I[max_i];
  PnI = table_P[max_i];
  
}

/*trouve la meilleure puissance dans le tableau de puissance*/ 
void MaxTable(float* X, int &max_i, float &max_v) 
{
  for ( int i = 0; i < sizeof(X); i++ )
    {
    if ( X[i] > max_v )
      {
      max_v = X[i];
      max_i = i;
      }
    }
}
