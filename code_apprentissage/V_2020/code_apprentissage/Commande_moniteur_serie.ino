/*
 * Cette partie du programme à pour but d'envoyer des commandes au hacheur par l'intermédiaire du moniteur série
 * Pour effectuer la lecture, on appelera la fonction à plusieurs étapes du programmes
 */

/*Initialisation des variables*/
byte  incomingByte;                  //variables pour commande moniteur série 


/*Fonction envoi de commande au hacheur*/
void commande_moniteur()
{ 
  if (Serial.available() > 0) //lecture dans le moniteur série
   {
    incomingByte = Serial.read();
    if((incomingByte=='O')||(incomingByte=='o')) {commande=0;}     //On arrete le programme et on hache à un certains rapport cyclique prédéfini pour la mise en sécurité de l'éolienne
    if((incomingByte=='I')||(incomingByte=='i')) {commande=1;}    //On début le programme
    if((incomingByte=='F')||(incomingByte=='f')) {commande = 2;}  //On active le frein de l'éolienne
    if((incomingByte=='L')||(incomingByte=='l')) {commande = 3;}  //L'éolienne tourne en roue libre
    if((incomingByte=='D')||(incomingByte=='l')) {commande = 5;}  //Lance le demarreur de l'éolienne 
    }
}   
