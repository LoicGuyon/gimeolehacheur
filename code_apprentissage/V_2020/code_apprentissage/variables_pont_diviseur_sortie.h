/*
 * Ce fichier contient les variables de la mesure de tension en sortie du hacheur
 */

//Variables entrée
int sortie_Vs_hacheur = 33;    // On lit sur une entrée
float sensorValue_Vs;       // valeur du pont diviseur de la tension de sortie 

//Variables classement tableau
const int nb_valeurs_Vs = 11; // nombre de valeurs dans le tableau
int tab_Vs[nb_valeurs_Vs];    //tableau regroupant les valeurs de tension 
float mediane_Vs, tmp_Vs;   //variables tableaux pont diviseur de tension sortie hacheur 

//Variables sortie
float tension_sortie;            //valeur tension sortie hacheur
