/*
 * Ce fichier contient les varibles pour le fonctionnement de la fonctionnement stabilite
 */

//compteur
int compt1 = 1;
int tempo1 = 10000;					        //a definir
float courant_stable;       //variable sortie
float temps_stabilite;
int temps_moniteur = 0;

//Tension
float Uin;
float Uout;
//int Uconsigne = 14;				  // a definir tension que doit attendre Uout pour charger la batterie // doit se trouver entre 24V et 28V
int Ulimite = 30;           //a definir 


//MOSFETs
float alpha;                  //rapport cyclique 
float alpha_step = 0.01;          //pas du alpha 
int mode_abaisseur = 1;     //activation du mode abaisseur 
int mode_elevateur = 0;     //désactivation du mode élévateur 
int frequence = 1000;       //fréquence de hachage 
int alpha_elevateur=0;    //rapport cyclique MOSFET SERIE (attention il est compris entre 0 et 255 ce qui correspond à 0% et 100%)
int alpha_abaisseur=0; //rapport cyclique MOSFET PARALELLE (attention il est compris entre 0 et 255 ce qui correspond à 0% et 100%)
int GPIO_mosfet_serie=23; // sortie PIN GPIO23 MOSFET ABAISSEUR
int GPIO_mosfet_parallele=22; //sortie GPIO22 MOSFET ELEVATEUR
float alpha_abaisseur_reel=0; //pour gérer les valeurs décimales...
