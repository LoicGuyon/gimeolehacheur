/*
 * Ce fichier contient les variables pour la partie puissance optimale
 */

float umin = 35;       //à définir 

float umax = 60;       //à définir

float upas = 1;       //à definir


int utaille = 0;       // initialisation taille du tableau
int tps_stabilisation;
int max_i = -1;                                                       // par défaut, l'indice du plus grand est -1 ! cette valeur sera mise à  jour par la fonction MaxTable
float max_value = -1;                                                 // par défaut, la valeur max vaut -1 ! cette valeur sera mise à  jour par la fonction MaxTable
