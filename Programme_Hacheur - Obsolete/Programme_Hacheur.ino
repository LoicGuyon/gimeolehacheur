/*
 * Ce programme permettra de contrôler l'éolienne
 */

//Reste à définir la vitesse de rotation de la deconnexion du demarreur dans variables programme_hacheur
//Reste à définir la vitesse_rotation_deconnexion pour activation du frein dans variables programme hacheur
//Reste à définir la vitesse_vent_demarreur pour voir quand est-ce que l'on active le demarreur

//déclaration des variables
#include "variables_pont_diviseur_entree.h"         //on inclut la bibliothèque contenant les variables de mesure de la tension en entree du hacheur
#include "variables_pont_diviseur_sortie.h"         //on inclut la bibliothèque contenant les variables de mesure de la tension en sortie du hacheur
#include "variables_mesure_courant_entree.h"        //on inclut la bibliothèque contenant les variables de mesure du courant d'entree
#include "variables_mesure_courant_sortie.h"        //on inclut la bibliothèque contenant les variables de mesure du courant de sortie
#include "variables_fonction_stabilite.h"           //on inclut la bibliothèque contenant les variables utilisées dans la fonction stabilité
#include "variables_programme_hacheur.h"            //on inclut la bibliothèque contenant les variables utilisées dans le programme du hacheur


// définition des prototypes de fonction
void commande_moniteur();
void puissance_optimale(float &UnI, float &InI, float &PnI, int &tps_stabilisation);
void mesure_tension_entree(float &tension_entree);
void mesure_courant_entree(float &courant_Iin);
void mesure_tension_sortie(float &tension_sortie);
void mesure_courant_sortie(float &courant_Iout);
void MaxTable(float* X, int &max_i, float &max_v);
void stabilite(float Uconsigne, float &courant_Iin, int &temps_total);

void setup() {
  
 Serial.begin(9600);                                //vitesse d'affichage du moniteur série

 //Demarreur
 demarrage = 1;                              //variable qui lancera la partie demarrage de l'éolienne au début 
 pinMode(moteur,INPUT);                             //le moteur du demarreur est une entrée
 pinMode(electro_aimant,INPUT);                     //l'electro_aimant du demarreur est une entrée

 //variables acquisition informations capteurs
 pinMode(entree_Ve_hacheur, INPUT);                //mesure tension entrée est une entrée
 pinMode(sortie_Vs_hacheur, INPUT);                //mesure tension sortie est une entrée
 
 pinMode(entree_Ie, INPUT);                        //mesure courant entrée est une entrée 
 pinMode(sortie_Is, INPUT);                        //mesure courant sortie est une entrée
 
 pinMode(vitesse_vent,INPUT);                      //La vitesse du vent est une entrée
 pinMode(vitesse_rotation,INPUT);                  //La vitesse de rotation est une entrée 

 //Variables de commande des MOSFET
 pinMode(GPIO_mosfet_serie,OUTPUT);                //MOSFET est une sortie

//Variable de commande du relai qui commande le frein 
 pinMode(relai_charge,OUTPUT);                     //relai de charge est une sortie 
 digitalWrite(relai_charge,LOW);                   //initialisation : désactivité

 //initialisation des PWM
 ledcSetup(1,1000, 8);                            // definition du pwm n°1  frequence est la fréquence du Hacheur et le rapport cyclique est codé sur 8 bits soit 256 valeurs possibles.
 ledcAttachPin(GPIO_mosfet_serie, 1);             // affectation du pwm n°1 à la pin GPIO_mosfet_serie. Attention toutes les pin de l'ESP32 ne sont pas connectées aux CAN internes de l'ESP32. Si on veut utiliser le WIFI, un seul CAN peut fonctionner....

 //initialisation des deux rapports cycliques
  ledcWrite(1, alpha_abaisseur);                 //abaisseur (serie)

}

void loop() {
    //Acquistion des vitesses de rotation et de vent 
    mesure_vitesse_rotation();
    mesure_vitesse_vent();
    //Affichage de la vitesse de rotation dans le moniteur série
    Serial.print("Vitesse rotation éolienne : ");
    Serial.print(tr_minu_rotation);
    Serial.println(" tr/min");

    //Affichage de la vitesse du vent dans le moniteur série
    Serial.print("Vitesse du vent : ");
    Serial.print(vitesse_vent);
    Serial.println(" km/h");
    Serial.println("");

    while (vitesse_vent < vitesse_vent_demarreur)   //On demarre le programme que lorsque la souflerie est demarrée 
    {
       mesure_vitesse_vent();                         // On acquiert la vitesse du vent
      //Affichage de la vitesse du vent dans le moniteur série
      Serial.print("Vitesse du vent : ");
      Serial.print(vitesse_vent);
      Serial.println(" km/h");
      Serial.println("");
    }
    
  demarreur(); //On lance le demarreur de l'éolienne

 //test de la deconnection de la charge 
 while (vitesse_rotation<=vitesse_rotation_deconnexion)
 {
    //acquisition des données 
    //Acquisition du courant d'entrée du hacheur
    mesure_courant_entree(courant_entree);                             //mesure courant entrée

    //acquisition de la vitesse de rotation de l'éolienne 
    mesure_vitesse_rotation();                                        //mesure vitesse de rotation
    
    //On affiche les valeurs
    Serial.print("Courant en entrée : ");
    Serial.print(courant_entree);
    Serial.println(" A");
    
    //On affiche la vitesse de rotation
    Serial.print("Vitesse rotation éolienne : ");
    Serial.print(tr_minu_rotation);
    Serial.print(" tr/min");

    U_in_opt = (courant_entree*courant_entree) + courant_entree + 3;  //Détermination tension optimale en fonction du courant d'entrée  //mettre équation récupérée d'après le programme puissance optimale
    U_c = U_in_opt;                                                  //Unouvelleconsigne = tension optimale
    U_ac = tension_entree + U_sl;                                    //Uancienneconsigne + pas de l'ancienne consigne 


    if(U_c>U_ac)                                                      //Si la tension_consigne augmente
    {
      U_c = U_in_opt;                                                 //La tension consigne est la tension optimale obtenue d'après l'équation                                              
      U_nc = min(U_c,U_ac + U_sl);                                    // La nouvelle consigne est le mini entre la tension consigne et celle de l'ancienneconsigne + pas 
     
    }
    
    else
    {
      U_nc = max(U_nc, U_ac - U_sl);                                  //On prend le maxi entre la nouvelleconsigne et l'ancienne -pas 
    }
  
  stabilite();                                                        //On appelle la fonction stabilité qui va hacher

  for(int i; i<50; i++)
  {
    //tempo 
  }
   mesure_vitesse_rotation();                                      //mesure vitesse de rotation
 }
  
  digitalWrite(relai_charge,HIGH);                              //activation du frein en cas de la deconnexion de la charge
  
}
