/*
 * cette partie du programme mesure le courant en sortie du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_courant_sortie(float &courant_Iout)
{
  //on classe 21 valeurs dans un tableau
  for (int i_Is=0; i_Is<21; i_Is++)
  {
    sensorValue_Is = analogRead(sortie_Is); //lecture de la valeur du courant de sortie 
    tab_Is[i_Is]= sensorValue_Is;
  }
  
  for (int i_Is=0; i_Is<21; i_Is++)
  {
    for (int j_Is=i_Is+1;j_Is<21;j_Is++)
    {
      //on classe les valeurs par ordre croissant
      if (tab_Is[j_Is]<tab_Is[i_Is])
      { //bisousfesse
        tmp_Is=tab_Is[i_Is];                                    //On lit la valeur de tab_Ie[i]
        tab_Is[i_Is]=tab_Is[j_Is];                              //La valeur de tab_Ie[i] prend la valeur de tab_Ie{j]
        tab_Is[j_Is]=tmp_Is;                                    //tab_Ie[j] prend la précédente valeur de tab_Ie[i]
      }
    }  
  }
  
mediane_Is=tab_Is[10];                                        //la valeur médiane correspond à ma 10ème valeur

courant_Iout = (float)((((mediane_Is+173.59)/1280.9)*1000)); //Formule CAN, conversion mV en V
courant_Iout=((courant_Iout-570)/60)-offset;                    //Offset de 570, 60mV/A, offset de 0.6V

}
