/*
 * Cette partie du programme mesure le courant en entrée du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_courant_entree(float &courant_Iin)
{
  //on lit 21 valeurs et on les places dans un tableau
  for (int i_Ie=0; i_Ie<21; i_Ie++)
  {
    sensorValue_Ie = analogRead(entree_Ie);                     //lecture du courant d'entrée
    tab_Ie[i_Ie]= sensorValue_Ie;                               //on place la valeur du courant d'entrée dans le tableau
  }

  //On classe les valeurs du tableau dans l'ordre croissant   
  for (int i_Ie=0; i_Ie<21; i_Ie++)
  {
     for (int j_Ie=i_Ie+1;j_Ie<21;j_Ie++)
     {
        //on classe les valeurs par ordre croissant
        if (tab_Ie[j_Ie]<tab_Ie[i_Ie])
        { 
          tmp_Ie=tab_Ie[i_Ie];                                      //On lit la valeur de tab_Ie[i]
          tab_Ie[i_Ie]=tab_Ie[j_Ie];                                //La valeur de tab_Ie[i] prend la valeur de tab_Ie{j]
          tab_Ie[j_Ie]=tmp_Ie;                                      //tab_Ie[j] prend la précédente valeur de tab_Ie[i]
        }
      }  
   }

mediane_Ie=tab_Ie[10];                                          //la valeur médiane correspond à ma 10ème valeur
courant_Iin = (float)((((mediane_Ie+173.59)/1280.9)*1000));     //Formule CAN, conversion mV en V
courant_Iin=((courant_Iin-570)/60)-offset;                         //Offset de 570, 60mV/A, offset de 0.6
courant_entree=courant_Iin;
                                              
}
