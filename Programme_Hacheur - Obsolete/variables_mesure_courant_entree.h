/*
 * ce fichier contient les variables permettant la lecture du courant d'entree du hacheur
 */


//Variables entrée
int entree_Ie = 38;            // On lit sur le pin 38
float sensorValue_Ie;         // valeur du capteur de courant entr�e 

//Variables classement tableau 
const int nb_valeurs_Ie = 21; // nombre de valeurs dans le tableau
int tab_Ie[nb_valeurs_Ie];   // tableau regroupant les valeurs de courant
float mediane_Ie, tmp_Ie; // variables tableaux capteur de courant 

//Offset capteurs de courant
float offset = 0.6;     //variables qui donne l'offset du capteur de courant Attention il change en fonction de la température ambiante

//Variables Sortie 
float courant_Iin; 
float courant_entree;       // valeur courant entr�e
