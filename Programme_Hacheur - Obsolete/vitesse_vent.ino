

void mesure_vitesse_vent() {
int tr_minu_vent
// Mesure la durée de l'impulsion haute (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_haut_vent = pulseIn(vitesse_vent, HIGH);
interrupts();

// Mesure la durée de l'impulsion basse (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_bas_vent = pulseIn(vitesse_vent, LOW);
interrupts();

// Calcul de la periode = etat haut + etat bas
float periode_vent = (etat_bas_vent + etat_haut_vent);
// Calcul de la frequence = 1 / periode
float frequence_vent = (1/ (periode_vent*0.01));
if (etat_bas_vent ==0 && etat_haut_vent==0)
{
  frequence_vent = 0;
  int vitesse_vent = tr_minu_vent*2.16; //2.16 est le rapport pour de l'anémomètre 1Hz = 2.16km/h
}
tr_minu_vent = frequence_vent*60;

/*Serial.println("Duree etat haut vent : ");
Serial.print(etat_haut_vent);
Serial.println("");
Serial.println("Duree etat bas vent : ");
Serial.print(etat_bas_vent);
Serial.println("");
Serial.println("Periode vent : ");
Serial.print(periode_vent);
Serial.println("");*/
/*Serial.print("Frequence vent : ");
Serial.print(frequence_vent);
Serial.println(" Hz");

Serial.print("Vitesse du vent : ");
Serial.print(vitesse_vent);
Serial.println(" km/h");*/

/*
Serial.print(tr_minu_vent);
Serial.print(" tr/min");
Serial.println("");
*/
//delay(1000);
}
