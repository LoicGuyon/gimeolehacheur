/* 
 * Cette partie du programme mesure la tension en enteée du hacheur
 * Pour filtrer le signal davantage (en plus des filtres RC), on mesure une série de valeur qu'on place dans un tableau et on en retire la médiane
 */

void mesure_tension_entree(float &tension_entree)            //le & veut dire que la variable est modifiable
{
                                                         //classe 11 valeurs dans un tableau 
  for (int i_Ve=0; i_Ve<11; i_Ve++)
  {
    sensorValue_Ve = analogRead(entree_Ve_hacheur);      //lecture Tension entrée     //mis en commentaire si l'on utilise la fonction random pour les tests
    tab_Ve[i_Ve]= sensorValue_Ve;
  }
 //On classe les valeurs du tableau dans l'ordre croissant
  for (int i_Ve=0; i_Ve<11; i_Ve++)
  {
    for (int j_Ve=i_Ve+1;j_Ve<11;j_Ve++)
    {
     if (tab_Ve[j_Ve]<tab_Ve[i_Ve])
     { 
      tmp_Ve=tab_Ve[i_Ve];
      tab_Ve[i_Ve]=tab_Ve[j_Ve];
      tab_Ve[j_Ve]=tmp_Ve;
     }
    }  
   }

  mediane_Ve=tab_Ve[5];                                             //on récupère la valeur médiane
  tension_entree = (float)(((mediane_Ve+173.59)/1280.9)*103.92); //utilisation de l'equation de la courbe caractéristique du CAN de l'ESP32 *Cst = coeff pont diviseur de tension + erreur resistance 


  
}
