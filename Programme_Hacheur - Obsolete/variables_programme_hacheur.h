/*
 * Ce fichier contient les variables pour la fonctionnement générale du hacheur
 */

//variables capteur de roation
int vitesse_rotation = 34;                              // Signal Analogique
int vitesse_rotation_deconnexion = 500;                //à déterminer
int tr_minu_rotation;

//variables anémomètre
int vitesse_vent = 35;                              // Signal Analogique
int vitesse_vent_deconnexion = 45;                 //à determiner

//variables relai court circuit
int relai_court_circuit = 18;


//Variable demarreur 
int demarrage;                                   //variable d'activation du demarreur
float vitesse_vent_demarreur = 2;               //Vitesse de vent à laquelle on debute le programme 
int moteur  = 26;                              //Port de commande du moteur
int electro_aimant = 25;                      //Port de commande de l'electro_aimant
float vitesse_deconnexion_demarreur = 12;    //à définir la vitesse de deconnexion du demarreur

//variable relai deconnexion de la charge 
int relai_charge = 19;


//variables comparaison
float U_c;
float U_nc;                            //U nouvelle consigne
float U_ac;                           //U ancienne consigne
float U_in_opt;                      //U optimale selon l'équation 
float U_sl = 0.1;                    //pas de U ancienne consigne 
