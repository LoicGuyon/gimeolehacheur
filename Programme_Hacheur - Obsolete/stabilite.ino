/*
 * cette partie du programme va reguler la tension d'entrée du hacheur partie hachage
 */
int commande = 0;
void stabilite()
{
  temps_total = 0;
  compt1=0;
  
  //commande_moniteur(); //lecture dans le moniteur série
  
  while (compt1<=tempo1 && temps_total <=50000 && commande==1)                            
  {
  //commande_moniteur(); //lecture dans le moniteur série
  temps_total ++;                                                       //on ajoute 1 à la variable a chaque répétition de la fonction
  temps_moniteur ++;

  mesure_tension_entree(tension_entree);                               //lit la tension d'entrée du hacheur
  mesure_tension_sortie(tension_sortie);                               //lit la tension de sortie du hacheur
  mesure_courant_entree(courant_entree);                               //lit le courant d'entree du hacheur
  mesure_courant_sortie(courant_sortie);                               //lit le courant de sortie
  //courant_Iin = courant_entree;
  //courant_sortie;

    if (tension_entree> Uconsigne)               //on augmente le alpha_abaisseur 
    {
      if(mode_abaisseur == 1 && mode_elevateur == 0)                         //si on est en mode abaisseur 
        {
           if (alpha_abaisseur + alpha_step < 240)                           //si inferieur à 80% du rapport cyclique 
              {
                alpha_abaisseur_reel = alpha_abaisseur_reel + alpha_step;   // on augmente le rapport cyclique de alpha_step             
              }
            if (alpha_abaisseur + alpha_step >= 240)                       //si supérieur à 80% du rapport cyclique 
              {
                alpha_abaisseur_reel = 240;                               //on met alpha à 80% du rapport cyclique      
              }
        }

        
    }
    else // (tension_entree < Uconsigne)
      {        
        if (mode_abaisseur ==1 && mode_elevateur ==0)
          {
          if (alpha_abaisseur - alpha_step > 0) 
            {
              alpha_abaisseur_reel = alpha_abaisseur_reel - alpha_step;        //on diminue le rapport cyclique de alpha_step
            }
          if (alpha_abaisseur - alpha_step <= 0)
            { 
              alpha_abaisseur_reel = 0;                                       //on met le rapport cyclique à zéro       
            }
          }
      }

      if (temps_total%1000==1)
      {Serial.print("Ui=");
      Serial.print(tension_entree);
      Serial.print("Ie=");
      Serial.print(courant_entree);
      
      Serial.print("A - a_buck=");
      Serial.print(alpha_abaisseur);
      Serial.print("b");
     // Serial.print(alpha_elevateur);  
      Serial.print(" - compteur:");
      Serial.println(compt1);
      
      }
 
     if (tension_entree >= (Uconsigne - (Uconsigne*0.03)) && tension_entree <= (Uconsigne + (Uconsigne * 0.03)))
      {
        compt1 = compt1+1;
      }
      else 
      {
        compt1 = 0;
      }

      
    alpha_abaisseur = alpha_abaisseur_reel;
    ledcWrite(1, alpha_abaisseur);                                     // met la valeur du rapport cyclique alpha sur le PWM ABAISSEUR
    //ledcWrite(2, alpha_elevateur);                                   // met la valeur du rapport cyclique alpha sur le PWM ELEVATEUR  

    
  }
}
