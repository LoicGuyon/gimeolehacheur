void mesure_vitesse_rotation() {

// Mesure la durée de l'impulsion haute (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_haut_rotation = pulseIn(vitesse_rotation, HIGH);
interrupts();

// Mesure la durée de l'impulsion basse (timeout par défaut de 1s)
noInterrupts();
unsigned long etat_bas_rotation = pulseIn(vitesse_rotation, LOW);
interrupts();

// Calcul de la periode = etat haut + etat bas
float periode_rotation = (etat_bas_rotation + etat_haut_rotation);
// Calcul de la frequence = 1 / periode
float frequence_rotation = (1/ (periode_rotation*0.01));
if (etat_bas_rotation ==0 && etat_haut_rotation==0)
{
  frequence_rotation = 0;
}
int tr_minu_rotation = frequence_rotation*60;
/*
Serial.println("Duree etat haut rotation: ");
Serial.print(etat_haut_rotation);
Serial.println("");
Serial.println("Duree etat bas rotation: ");
Serial.print(etat_bas_rotation);
Serial.println("");
Serial.println("Periode rotation: ");
Serial.print(periode_rotation);
Serial.println("");
Serial.println("Frequence rotation: ");
Serial.print(frequence_rotation);
Serial.println(" Hz");
Serial.print(tr_minu_rotation);
Serial.print(" tr/min");
Serial.println("");*/

//delay(1000);
}
