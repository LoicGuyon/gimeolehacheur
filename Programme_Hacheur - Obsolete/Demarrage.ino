/*
 * Cette partie du programme demarre l'éolienne en début de concours. 
 */

void demarreur ()
{
  while((demarrage ==1) && (vitesse_rotation <= vitesse_deconnexion_demarreur))
    {
      digitalWrite(moteur,HIGH);
      delay(1000);
      digitalWrite(electro_aimant,HIGH);
    }

   demarrage = 0;
   digitalWrite(electro_aimant,LOW);
   delay(1000);
   digitalWrite(moteur,LOW);
     
}
